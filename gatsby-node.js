exports.onCreateWebpackConfig = ({ stage, loaders, actions }) =>
{
    const config = {};

    if (stage === "build-html")
    {
        config.module = {
            rules: [
                {
                    test: /slick-carousel/,
                    use: loaders.null(),
                }
            ],
        };
    }

    /**
     * TODO: Check if this is still needed with later versions.
     * It's just a workaround from https://github.com/gatsbyjs/gatsby/issues/11934
     * error occurring in this version without the following: 'React-Hot-Loader: react-🔥-dom patch is not detected'
     */
    if (stage.startsWith("develop"))
    {
        config.resolve = {
            alias: {
                "react-dom": "@hot-loader/react-dom",
            },
        };
    }

    actions.setWebpackConfig(config);
};
