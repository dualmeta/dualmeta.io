module.exports = {
    plugins: [
        "gatsby-plugin-react-helmet",
        "gatsby-plugin-sass",
        "gatsby-plugin-typescript",
        "gatsby-plugin-postcss",
        {
            resolve: `gatsby-plugin-google-gtag-optin`,
            options: {
                trackingIds: [
                    "G-9LE6JS281S",
                ],
                // This object gets passed directly to the gtag config command
                // This config will be shared across all trackingIds
                gtagConfig: {
                    optimize_id: "GTM-MS25PKW",
                    anonymize_ip: true,
                    cookie_expires: 0,
                },
                // This object is used for configuration specific to this plugin
                pluginConfig: {
                    // Puts tracking script in the head instead of the body
                    head: false,
                    // Setting this parameter is also optional
                    respectDNT: true,
                    // Avoids sending pageview hits from custom paths
                    exclude: ["/preview/**", "/do-not-track/me/too/"],
                    // localStorage key that has to be set to true for plugin to load
                    optinKey: "gtag_optin", // default
                },
            },
        },
    ],
    siteMetadata: {
        title: "Dualmeta - Digitalisierung durch innovative Webentwicklung",
        description: "Dualmeta ist Ihr Partner im Raum Stuttgart für herausragende Webanwendungen, modernes Webdesign und smarte Appentwicklung.",
        url: "https://www.dualmeta.io",
        image: "/images/og_image.jpg",
        logo: "/images/Logo.png"
    }
};
