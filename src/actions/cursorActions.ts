import { ECursorRef } from "../utils/enums";
import { IStore } from "../utils/store";
import refActions from "./refActions";

const cursorStyles = require("../components/global/cursor/Cursor.module.sass");

export default {

    hoverCursors: (store: IStore) =>
    {
        const normalCursorRef = refActions.getRefById(store, ECursorRef.normalCursor);
        const laggyCursorRef = refActions.getRefById(store, ECursorRef.laggyCursor);

        if(!normalCursorRef || !laggyCursorRef)
        {
            return;
        }

        if(normalCursorRef.current.classList.contains(cursorStyles.normalHovered))
        {
            return;
        }

        normalCursorRef.current?.classList.add(cursorStyles.normalHovered);
        laggyCursorRef.current?.classList.add(cursorStyles.laggyHovered);
    },

    unHoverCursors: (store: IStore) =>
    {
        const normalCursorRef = refActions.getRefById(store, ECursorRef.normalCursor);
        const laggyCursorRef = refActions.getRefById(store, ECursorRef.laggyCursor);

        if(!normalCursorRef || !laggyCursorRef)
        {
            return;
        }

        normalCursorRef.current?.classList.remove(cursorStyles.normalHovered);
        laggyCursorRef.current?.classList.remove(cursorStyles.laggyHovered);
    }
};
