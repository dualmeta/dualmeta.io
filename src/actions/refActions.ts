import $ from "jquery";
import React from "react";

import { EScrollPoint } from "../utils/enums";
import { IStore } from "../utils/store";

const refActions = {

    addRef: (store: IStore, ref: React.RefObject<HTMLElement>) =>
    {
        store.refs.push(ref);
        store.notify();
    },

    getRefById: (store: IStore, refID): React.RefObject<HTMLElement> | null => store.refs.find(ref =>
    {
        if(!ref.current)
        {
            return;
        }
        return ref.current.id === refID;
    }),

    scrollToRef: (store, refID): void =>
    {
        if(typeof window === "undefined")
        {
            return;
        }

        const ref = refActions.getRefById(store, refID);

        if(!ref)
        {
            console.log("Ref id nicht gefunden");
            return;
        }

        let scrollDestination = ref.current.offsetTop - 150;

        if(ref.current.id === EScrollPoint.kontakt)
        {
            scrollDestination = ref.current.offsetTop + 40;
        }

        $("html, body").animate({ scrollTop: scrollDestination });
    }
};

export default refActions;
