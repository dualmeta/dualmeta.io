import { checkWebPFeature } from "../utils/checkWebPSupport";
import { EWebPSupport } from "../utils/enums";
import { IStore } from "../utils/store";

export const browserActions = {

    checkWebPSupport: (store: IStore) =>
    {
        if(typeof window !== "undefined")
        {
            checkWebPFeature("lossy", (feature, isSupported) =>
            {
                if(isSupported)
                {
                    store.webPSupport = EWebPSupport.supported;
                }
                else
                {
                    store.webPSupport = EWebPSupport.notSupported;
                }
                store.notify();
            });
        }
    }
};
