import { StaticQuery, graphql } from "gatsby";
import React, { FunctionComponent } from "react";
import { Helmet } from "react-helmet";
import { Organization, WithContext } from "schema-dts";

import favicon from "../../static/favicon.png";

const query = graphql`
  query SEO {
    site {
      siteMetadata {
        defaultTitle: title
        defaultDescription: description
        siteUrl: url,
        defaultImage: image,
        logo
      }
    }
  }
`;

interface IProps
{
    title?: string;
    description?: string;
    pathname?: string;
    image?: string;
}

const SEO: FunctionComponent<IProps> = props => (
    <StaticQuery
        query={query}
        render={(
            {
                site: {
                    siteMetadata: {
                        defaultTitle,
                        defaultDescription,
                        siteUrl,
                        defaultImage,
                        logo,
                    },
                },
            }
        ) =>
        {
            const seo = {
                title: props.title || defaultTitle,
                description: props.description || defaultDescription,
                image: `${siteUrl}${props.image || defaultImage}`,
                url: `${siteUrl}${props.pathname || "/"}`,
                logo: `${siteUrl}${logo}`,
            };

            const ldJson: WithContext<Organization> = {
                "@context": "https://schema.org",
                "@type": "Organization",
                "@id": "https://www.dualmeta.io",
                url: siteUrl,
                description: seo.description,
                image: seo.image,
                logo: seo.logo,
                name: "Dualmeta",
                address: {
                    "@type": "PostalAddress",
                    streetAddress: "Grabenstraße 35",
                    addressLocality: "Goeppingen",
                    addressRegion: "Baden-Württemberg",
                    postalCode: "73033",
                    addressCountry: "DE"
                },
                telephone: "+4915223687751",
                email: "hello@dualmeta.io"
            };

            return (
                <Helmet title={seo.title}>
                    <html lang={"de"}/>
                    <meta charSet="UTF-8"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
                    <meta name="description" content={seo.description}/>
                    <meta name="author" content={seo.url}/>
                    <link rel="canonical" href={seo.url}/>
                    <link rel="shortcut icon" type="image/x-icon" href={favicon}/>
                    <link rel="shortcut icon" type="image/png" href={favicon}/>
                    <link rel="shortcut icon" sizes="196x196" href={favicon}/>
                    <link rel="apple-touch-icon" href={favicon}/>
                    <meta name="theme-color" content="#c23e54"/>
                    <script type="application/ld+json">{JSON.stringify(ldJson)}</script>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image" content={seo.image}/>
                    <meta property="og:image:type" content="image/jpeg"/>
                    <meta property="og:image:alt" content={"A screenshot of the website"}/>
                    <meta property="og:locale" content="de_DE"/>
                    <meta property="og:url" content={seo.url}/>
                    <meta property="og:title" content={seo.title}/>
                    <meta property="og:description" content={seo.description}/>
                </Helmet>
            );
        }}
    />
);

export default SEO;
