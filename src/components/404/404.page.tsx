import React from "react";

import { IStore, IStoreProps } from "../../utils/store";
import { Cursor } from "../global/cursor/Cursor";
import { Header } from "../global/header/Header";

export class Page404 extends React.Component<IStoreProps, IStoreProps>
{
    public constructor(props: IStoreProps)
    {
        super(props);

        this.state = {
            store: props.store
        };

        props.store.subscribers.push((updatedStore: IStore) =>
        {
            this.setState({ store: updatedStore });
        });
    }

    public render(): React.ReactElement
    {
        const { store } = this.state;

        return (
            <div>
                <Cursor store={store}/>
                <Header store={store}/>
                <h1 style={{ marginTop: "25vh", textAlign: "center" }}>404 Page not found!</h1>
            </div>
        );
    }
}
