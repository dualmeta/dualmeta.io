import * as React from "react";
import { FunctionComponent, useEffect } from "react";

export const Calendly: FunctionComponent = () =>
{
    useEffect(() =>
    {
        const head = document.querySelector("head");
        const script = document.createElement("script");
        script.setAttribute("src", "https://assets.calendly.com/assets/external/widget.js");
        head.appendChild(script);
    });

    return (
        <div
            className="calendly-inline-widget"
            data-url="https://calendly.com/dualmeta/termin?primary_color=cc3354"
            style={{ minWidth: "320px", height: "100vh" }} />
    );
};

