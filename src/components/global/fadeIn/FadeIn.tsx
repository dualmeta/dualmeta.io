import * as React from "react";
import { FunctionComponent, useEffect, useState } from "react";

interface IProps
{
    className?: string;
    fadeInDelay?: number;
    fadeInSpeed?: number;
    isFadeInDisabled?: boolean;
}

export const FadeIn: FunctionComponent<IProps> = props =>
{
    const [isActive, setIsActive] = useState(false);

    const style = {
        opacity: 0,
        transition: "opacity " + String(props.fadeInSpeed || 1000) + "ms ease-in"
    };

    const active = {
        opacity: 1
    };

    useEffect(() =>
    {
        const timeout = setTimeout(() =>
        {
            setIsActive(true);
        }, props.fadeInDelay || 0);

        return () => clearTimeout(timeout);
    }, [props.fadeInDelay]);

    return (
        <div className={props?.className} style={ props.isFadeInDisabled ? null : Object.assign(style, isActive ? active : null)}>
            {props.children}
        </div>
    );
};
