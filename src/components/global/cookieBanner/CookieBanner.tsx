import { GTAG_OPTIN_KEY } from "gatsby-plugin-google-gtag-optin";
import React, { FunctionComponent, useEffect, useState } from "react";

import cursorActions from "../../../actions/cursorActions";
import { IClassProps } from "../../../utils/interfaces";
import { IStoreProps } from "../../../utils/store";
import { FadeIn } from "../fadeIn/FadeIn";

const styles = require("./CookieBanner.module.sass");

interface IProps extends IClassProps, IStoreProps {}

export const CookieBanner: FunctionComponent<IProps> = props =>
{
    const [isHidden, setIsHidden] = useState(false);
    
    useEffect(() => 
    {
        if(localStorage.getItem("banner_was_answered"))
        {
            setIsHidden(true);
        }
    }, []);
    
    const cookieBannerWasClicked = (): void => 
    {
        localStorage.setItem("banner_was_answered", "true");
        setIsHidden(true);
    };
    
    const allowCookies = (): void =>
    {  
        localStorage.setItem(GTAG_OPTIN_KEY, "true");
        cookieBannerWasClicked();
        // @ts-ignore
        if(typeof window.loadGtag == "function") 
        {
            // @ts-ignore
            window.loadGtag();
        }
    };

    return (
        <FadeIn fadeInSpeed={1700} className={styles.fadeInZIndex}>
            <div className={[styles.wrapper, props?.className, isHidden && styles.hide].join(" ")}>
                <div className={styles.content}>
                    <p>
                  Diese Internetseite verwendet Cookies und Google Analytics für die Analyse und Statistik.
                  Wir nutzen Cookies zu unterschiedlichen Zwecken, unter anderem zur Analyse und für personalisierte
                  Marketing-Mitteilungen. Weitere Informationen zur Verwendung deiner Daten findest du in unserer &nbsp;
                        <a
                            onMouseMove={() => cursorActions.hoverCursors(props.store)}
                            onMouseLeave={() => cursorActions.unHoverCursors(props.store)}
                        >Datenschutzerklärung</a>.
                    </p>
                    <div>
                        <button
                            onMouseMove={() => cursorActions.hoverCursors(props.store)}
                            onMouseLeave={() => cursorActions.unHoverCursors(props.store)}
                            onClick={() => allowCookies()}
                        >Akzeptieren
                        </button>
                        <button
                            onMouseMove={() => cursorActions.hoverCursors(props.store)}
                            onMouseLeave={() => cursorActions.unHoverCursors(props.store)}
                            onClick={() => cookieBannerWasClicked()}
                        >Ablehnen
                        </button>
                    </div>
                </div>
            </div>
        </FadeIn>
    );
};
