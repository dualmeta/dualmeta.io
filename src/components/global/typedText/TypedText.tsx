import React, { FunctionComponent, useEffect, useState } from "react";

const styles = require("./TypedText.module.sass");

interface IProps
{
    typedText: string;
    className?: string;
}

export const TypedText: FunctionComponent<IProps> = (props) =>
{
    const [text, setText] = useState("");

    useEffect(() =>
    {
        if(text === props.typedText)
        {
            return;
        }

        const charArray = Array.from(props.typedText);

        let counter = 0;

        const interval = setInterval(() =>
        {
            if(charArray[counter] === " ")
            {
                setText(t => t + charArray[counter]);
                counter++;
            }

            if(counter === charArray.length)
            {
                clearInterval(interval);
            }

            setText(t => t + charArray[counter]);

            counter++;

            if(counter === charArray.length)
            {
                clearInterval(interval);
            }
        }, 65);

        return () => clearInterval(interval);

    }, [props.typedText]);

    return <p className={props.className ? [styles.text, props.className].join(" ") : styles.text}>{text}</p>;
};
