import { Link } from "gatsby";
import React, { FunctionComponent } from "react";

import cursorActions from "../../../actions/cursorActions";
import { IStoreProps } from "../../../utils/store";

const styles = require("./Footer.module.sass");

interface IProps extends IStoreProps
{
    className?: string;
}

export const Footer: FunctionComponent<IProps> = props => (
    <div className={props.className ? [styles.wrapper, props.className].join(" ") : styles.wrapper}>
        <nav>
            <div className={styles.nav}>
                <Link
                    className={styles.link}
                    to={"/impressum/"}
                    onMouseEnter={() => cursorActions.hoverCursors(props.store)}
                    onMouseLeave={() => cursorActions.unHoverCursors(props.store)}>
                    Impressum
                </Link>
                <Link
                    className={styles.link}
                    to={"/datenschutz/"}
                    onMouseEnter={() => cursorActions.hoverCursors(props.store)}
                    onMouseLeave={() => cursorActions.unHoverCursors(props.store)}>
                    Datenschutz
                </Link>
                <Link
                    className={styles.link}
                    to={"/agb/"}
                    onMouseEnter={() => cursorActions.hoverCursors(props.store)}
                    onMouseLeave={() => cursorActions.unHoverCursors(props.store)}>
                    AGB
                </Link>
                <a
                    target={"_blank"}
                    rel={"noopener noreferrer"}
                    href={"https://www.linkedin.com/company/dualmeta"}
                    className={styles.link}
                    onMouseEnter={() => cursorActions.hoverCursors(props.store)}
                    onMouseLeave={() => cursorActions.unHoverCursors(props.store)}>
                    LinkedIn
                </a>
            </div>
        </nav>
    </div>
);
