import React, { Fragment, FunctionComponent, MutableRefObject, useEffect, useRef, useState } from "react";

import cursorActions from "../../../actions/cursorActions";
import refActions from "../../../actions/refActions";
import { ECursorRef } from "../../../utils/enums";
import { IStoreProps } from "../../../utils/store";

const styles = require("./Cursor.module.sass");

const normalOffset = parseInt(styles.normalSize) / 2;
const laggyOffset = parseInt(styles.laggySize) / 2;

export const Cursor: FunctionComponent<IStoreProps> = ({ store }) =>
{
    const [isVisible, setIsVisible] = useState(false);
    const [hasAddedRefs, setHasAddedRefs] = useState(false);

    const normalCursor: MutableRefObject<HTMLDivElement> = useRef(null);
    const laggyCursor: MutableRefObject<HTMLDivElement> = useRef(null);

    let lastTouchTimestamp;
    let isNeedForRAF = true;  // to prevent redundant rAF calls

    useEffect(() =>
    {
        if(isVisible && !hasAddedRefs && normalCursor?.current)
        {
            refActions.addRef(store, normalCursor);
            refActions.addRef(store, laggyCursor);
            setHasAddedRefs(true);
        }
    }, [isVisible]);

    const translateCursor = (mouseX: number, mouseY: number): void =>
    {
        isNeedForRAF = true;  // rAF consumes the movement instruction a new one can come

        laggyCursor.current.setAttribute("style", `transform: translateX(${mouseX - laggyOffset}px) translateY(${mouseY - laggyOffset}px)`);
        normalCursor.current.setAttribute("style", `transform: translateX(${mouseX - normalOffset}px) translateY(${mouseY - normalOffset}px)`);
    };

    const onMouseMove = (e: MouseEvent): void =>
    {
        if(e.movementY > 0 || e.movementY < 0 || e.movementX > 0 || e.movementX < 0)
        {
            // prevent mousemove on touch devices
            if(lastTouchTimestamp && e.timeStamp - lastTouchTimestamp < 1000)
            {
                return;
            }

            if(!isVisible)
            {
                setIsVisible(true);
            }

            if(isNeedForRAF)
            {
                isNeedForRAF = false;  // no need to call rAF up until next frame
                requestAnimationFrame(() => translateCursor(e.clientX, e.clientY));  // request 60fps animation
            }
        }
    };

    const onTouchStart = (e: TouchEvent): void =>
    {
        lastTouchTimestamp = e.timeStamp;
    };

    useEffect(() =>
    {
        document.addEventListener("mousemove", onMouseMove, { passive: true });
        document.addEventListener("touchstart", onTouchStart, { passive: true });
        return () => document.removeEventListener("mousemove", onMouseMove);
    }, []);

    useEffect(() =>
    {
        document.addEventListener("scroll", () => cursorActions.unHoverCursors(store), { passive: true });

        return () => document.removeEventListener("scroll", () => cursorActions.unHoverCursors(store));
    }, []);

    if(!isVisible)
    {
        return null;
    }

    return (
        <Fragment>
            <div ref={normalCursor} id={ECursorRef.normalCursor} className={[styles.cursor, styles.normal].join(" ")}/>
            <div ref={laggyCursor} id={ECursorRef.laggyCursor} className={[styles.cursor, styles.laggy].join(" ")}>
                <div/>
            </div>
        </Fragment>
    );
};
