import React, { FunctionComponent, Fragment, useEffect, useState } from "react";

import { EScrollPoint } from "../../../../utils/enums";
import { IStoreProps } from "../../../../utils/store";
import { eType, NavListItem } from "../../navListItem/NavListItem";

const styles = require("./FullScreenMenu.module.sass");

interface IProps extends IStoreProps
{
    isActive: boolean;
    closeMenu: () => void;
}

export const FullScreenMenu: FunctionComponent<IProps> = props =>
{
    const [isHomepage, setIsHomepage] = useState(false);

    useEffect(() =>
    {
        if(window.location.pathname === "/")
        {
            setIsHomepage(true);
        }
    }, []);

    return (
        <div className={props.isActive ? [styles.wrapper, styles.wrapperActive].join(" ") : styles.wrapper}>
            <nav className={styles.linksWrapper}>
                <NavListItem store={props.store} type={eType.gatsbyLink} to={"/"} text={"Home"} onClick={props.closeMenu}/>
                {isHomepage
                    ?
                    <Fragment>
                        <NavListItem store={props.store} type={eType.scrollLink} to={EScrollPoint.leistungen} text={"Leistungen"}
                            onClick={props.closeMenu}/>
                        <NavListItem store={props.store} type={eType.scrollLink} to={EScrollPoint.litedesk} text={"Referenzen"}
                            onClick={props.closeMenu}/>
                        <NavListItem store={props.store} type={eType.scrollLink} to={EScrollPoint.warumDualmeta} text={"Vorteile"}
                            onClick={props.closeMenu}/>
                        <NavListItem store={props.store} type={eType.scrollLink} to={EScrollPoint.rezensionen} text={"Bewertungen"}
                            onClick={props.closeMenu}/>
                        <NavListItem store={props.store} type={eType.scrollLink} to={EScrollPoint.kontakt} text={"Kontakt"}
                            onClick={props.closeMenu}/>
                    </Fragment>
                    :
                    null
                }
            </nav>
        </div>
    );
};

