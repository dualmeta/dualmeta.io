import { Link } from "gatsby";
import React, { FunctionComponent, useEffect, useState } from "react";

import Logo from "../../../../static/images/Logo.png";
import LogoWebP from "../../../../static/images/Logo.webp";
import cursorActions from "../../../actions/cursorActions";
import { IStoreProps } from "../../../utils/store";
import { InfoComponent } from "../infoComponent/InfoComponent";
import { BurgerMenu } from "./burgerMenu/BurgerMenu";
import { FullScreenMenu } from "./fullScreenMenu/FullScreenMenu";

const styles = require("./Header.module.sass");

export const Header: FunctionComponent<IStoreProps> = ({ store }) =>
{
    const [isMenuActive, setIsMenuActive] = useState(false);
    const [isShrinked, setIsShrinked] = useState(false);

    const scrollFunction = (): void => setIsShrinked(window.pageYOffset > 0);

    useEffect(() =>
    {
        window.addEventListener("scroll", scrollFunction, { passive: true });
        setIsShrinked(window.pageYOffset > 0);

        return () => window.removeEventListener("scroll", scrollFunction);
    }, []);

    return (
        <div className={isShrinked ? [styles.wrapper, styles.wrapperShrinked].join(" ") : styles.wrapper}>
            <Link to={"/"}>
                <picture>
                    <source srcSet={LogoWebP} type="image/webp"/>
                    <source srcSet={Logo} type="image/jpg"/>
                    <img className={styles.logo}
                        src={Logo}
                        alt={"Dualmeta Logo"}
                        onMouseEnter={() => cursorActions.hoverCursors(store)}
                        onMouseLeave={() => cursorActions.unHoverCursors(store)}
                    />
                </picture>
            </Link>
            <BurgerMenu
                store={store}
                isMenuActive={isMenuActive}
                menuLabel={true}
                className={[styles.burgerMenu, isShrinked ? styles.burgerMenuShrinked : null].join(" ")}
                setMenuVisibility={(bool) => setIsMenuActive(bool)}/>
            <FullScreenMenu store={store} closeMenu={() => setIsMenuActive(false)} isActive={isMenuActive}/>
            <InfoComponent className={styles.info} icon={"email"} store={store}>
                <a href={"mailto:hello@dualmeta.io"} className={styles.email}>hello@dualmeta.io</a>
            </InfoComponent>
        </div>
    );
};
