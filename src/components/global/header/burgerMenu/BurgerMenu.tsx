import React, { FunctionComponent, useEffect, useState } from "react";

import cursorActions from "../../../../actions/cursorActions";
import { IStoreProps } from "../../../../utils/store";

const styles = require("./BurgerMenu.module.sass");

interface IProps extends IStoreProps
{
    menuLabel: boolean;
    className?: string;
    isMenuActive: boolean;
    setMenuVisibility: (bool: boolean) => void;
}

export const BurgerMenu: FunctionComponent<IProps> = props =>
{
    const [isHovered, setIsHovered] = useState(false);

    const menuLabel = <p className={styles.menuLabel}>MENÜ</p>;

    useEffect(() =>
    {
        if(isHovered)
        {
            if(props.isMenuActive)
            {
                cursorActions.unHoverCursors(props.store);
            }
            else
            {
                cursorActions.hoverCursors(props.store);
            }
        }
        else
        {
            if(!props.isMenuActive)
            {
                cursorActions.unHoverCursors(props.store);
            }
        }
    }, [props.isMenuActive, isHovered]);

    return (
        <div
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            className={props.className ? [styles.outerWrapper, props.className].join(" ") : styles.outerWrapper}
            onClick={() => props.setMenuVisibility(!props.isMenuActive)}>
            <div className={props.isMenuActive ? [styles.burgerWrapper, styles.active].join(" ") : styles.burgerWrapper}>
                <div className={styles.line}/>
            </div>
            {props.menuLabel && !props.isMenuActive ? menuLabel : null}
        </div>
    );
};
