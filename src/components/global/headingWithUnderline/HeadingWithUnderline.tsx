import * as React from "react";
import { FunctionComponent } from "react";

const styles = require("./HeadingWithUnderline.module.sass");

interface IProps
{
    text: string;
}

export const HeadingWithUnderline: FunctionComponent<IProps> = props =>
    <h1 className={styles.heading}>
        {props.text}
    </h1>;
