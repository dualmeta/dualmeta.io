import React, { FunctionComponent } from "react";

const styles = require("./DummyBrowserControlsBar.module.sass");

interface IProps
{
    className?: string;
}

export const DummyBrowserControlsBar: FunctionComponent<IProps> = props => (
    <div className={[styles.browserBar, props.className ? props.className : null].join(" ")}>
        <div className={styles.dummyControlsWrapper}>
            <div/>
            <div/>
            <div/>
        </div>
    </div>
);

