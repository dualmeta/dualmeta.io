import React, { FunctionComponent, useEffect, useState } from "react";

import cursorActions from "../../../actions/cursorActions";
import { IStore } from "../../../utils/store";
import { Icon } from "../icon/Icon";

const styles = require("./Button.module.sass");

export enum eButtonType { outlined, filled}

export interface IResponsiveText
{
    text: string;
    breakpoint: number;
}

interface IProps
{
    type: eButtonType;
    text: string;
    responsiveText?: IResponsiveText;
    className?: string;
    icon?: string;
    onClick?: () => void;
    hideAt?: number;
    withLink?: boolean;
    target?: string;
    store?: IStore;
}

export const Button: FunctionComponent<IProps> = props =>
{
    const [text, setText] = useState<string>(props.text);
    const [windowWidth, setWindowWidth] = useState<number>(0);

    const updateWindowWidth = (): void => setWindowWidth(window.innerWidth);

    useEffect(() =>
    {
        updateWindowWidth();
        window.addEventListener("resize", updateWindowWidth, { passive: true });

        return () => window.removeEventListener("resize", updateWindowWidth);
    }, []);

    useEffect(() =>
    {
        if(props.responsiveText)
        {
            setText(windowWidth > props.responsiveText.breakpoint ? props.text : props.responsiveText.text);
        }
    }, [text, props.text, props.responsiveText, windowWidth]);

    const icon = <Icon icon={props.icon} className={styles.icon}/>;

    if(props.hideAt && windowWidth < props.hideAt)
    {
        return null;
    }

    return (
        props.withLink
            ?
            <a className={[
                styles.button,
                props.type === eButtonType.filled ? styles.filled : styles.outlined,
                props.className ? props.className : null
            ].join(" ")} onClick={props?.onClick}
            href={props?.target}
            target={"blank"}
            onMouseEnter={() => cursorActions.hoverCursors(props.store)}
            onMouseLeave={() => cursorActions.unHoverCursors(props.store)}>
                {props.icon ? icon : null}
                {text}
            </a>
            :
            <div className={[
                styles.button,
                props.type === eButtonType.filled ? styles.filled : styles.outlined,
                props.className ? props.className : null
            ].join(" ")}
            onClick={props?.onClick}
            onMouseEnter={() => cursorActions.hoverCursors(props.store)}
            onMouseLeave={() => cursorActions.unHoverCursors(props.store)}>
                {props.icon ? icon : null}
                {text}
            </div>
    );
};
