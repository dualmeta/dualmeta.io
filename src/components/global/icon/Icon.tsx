import React, { FunctionComponent } from "react";

interface IProps extends React.ComponentProps<"i">
{
    icon: string;
    className?: string;
}

export const Icon: FunctionComponent<IProps> = props => (
    <i {...props} className={["material-icons", props.className ? props.className : ""].join(" ")}>
        {props.icon}
    </i>
);
