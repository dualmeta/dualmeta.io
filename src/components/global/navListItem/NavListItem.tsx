import { Link } from "gatsby";
import React, { FunctionComponent } from "react";

import refActions from "../../../actions/refActions";
import { IStoreProps } from "../../../utils/store";

const styles = require("./NavListItem.module.sass");

export enum eType { a, gatsbyLink, scrollLink}

interface IProps extends IStoreProps
{
    type: eType;
    to: string;
    text: string;
    onClick?: () => void;
    className?: string;
}

export const NavListItem: FunctionComponent<IProps> = props =>
{
    if(props.type === eType.gatsbyLink)
    {
        return (
            <Link
                activeClassName={styles.active}
                className={props.className ? [styles.link, props.className].join(" ") : styles.link}
                to={props.to}
                onClick={props.onClick}>
                {props.text}
            </Link>
        );
    }
    else if(props.type === eType.scrollLink)
    {
        return (
            <a className={props.className ? [styles.link, props.className].join(" ") : styles.link} onClick={() =>
            {
                refActions.scrollToRef(props.store, props.to);
                props.onClick();
            }}>
                {props.text}
            </a>
        );
    }
    else
    {
        return (
            <a className={props.className ? [styles.link, props.className].join(" ") : styles.link} onClick={props.onClick} href={props.to}>
                {props.text}
            </a>
        );
    }
};
