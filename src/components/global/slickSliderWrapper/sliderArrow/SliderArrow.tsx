import React, { FunctionComponent } from "react";

import cursorActions from "../../../../actions/cursorActions";
import { IStoreProps } from "../../../../utils/store";

const styles = require("./SliderArrow.module.sass");

export enum eArrowType { prev, next }

interface IProps extends IStoreProps
{
    type: eArrowType;
    className: string;
    accentColor?: string;
    margin?: string;
    translateY?: number;
}

export const SliderArrow: FunctionComponent<IProps> = props => (
    <div
        style={{
            margin: props.margin ? `0 ${props.margin}` : null,
            backgroundColor: props.accentColor ? props.accentColor : undefined,
            transform: props.translateY ? `translateY(calc(-50% - ${props.translateY}px))` : undefined
        }}
        className={[
            props.className,
            styles.arrow,
            props.type === eArrowType.next ? styles.next : styles.prev
        ].join(" ")}
        onMouseLeave={() => cursorActions.unHoverCursors(props.store)}
        onMouseEnter={() => cursorActions.hoverCursors(props.store)}>
        <svg
            style={{ transform: ["scale(1.4)", props.type === eArrowType.next ? "rotate(180deg)" : null].join(" ") }}
            xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
            <path d="M0 0h24v24H0V0z" fill="none"/>
            <path
                d="M14.91 6.71c-.39-.39-1.02-.39-1.41 0L8.91 11.3c-.39.39-.39 1.02 0 1.41l4.59 4.59c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L11.03 12l3.88-3.88c.38-.39.38-1.03 0-1.41z"/>
        </svg>
    </div>
);
