import React, { Fragment, FunctionComponent } from "react";

const styles = require("./GradientOverlay.module.sass");

export interface IGradientOverlayProps
{
    rightAndLeftSideWidth?: string;
    colorFrom: string;
    colorTo: string;
}

export const GradientOverlay: FunctionComponent<IGradientOverlayProps> = props => (
    <Fragment>
        <div
            style={{
                width: props.rightAndLeftSideWidth ? props.rightAndLeftSideWidth : undefined,
                background: `transparent linear-gradient(90deg, ${props.colorFrom} 14%, ${props.colorTo} 100%) 0 0 no-repeat padding-box`
            }}
            className={[styles.gradientOverlay].join(" ")}/>
        <div
            style={{
                width: props.rightAndLeftSideWidth ? props.rightAndLeftSideWidth : undefined,
                background: `transparent linear-gradient(90deg, ${props.colorTo} 0%, ${props.colorFrom} 86%) 0 0 no-repeat padding-box`
            }}
            className={[styles.gradientOverlay, styles.rightGradientOverlay].join(" ")}/>
    </Fragment>
);
