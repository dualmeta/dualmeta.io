import $ from "jquery";
import React, { FunctionComponent, useEffect } from "react";
import "slick-carousel";

import { IStore } from "../../../utils/store";
import { GradientOverlay, IGradientOverlayProps } from "./gradientOverlay/GradientOverlay";
import { eArrowType, SliderArrow } from "./sliderArrow/SliderArrow";

const styles = require("./SlickSliderWrapper.module.sass");

import "./slick.sass";

interface IProps
{
    uniqueSliderClass: string;
    arrowsMargin?: string;
    dotsTranslateY?: number;
    arrowsTranslateY?: number;
    sliderClassName?: string;
    sliderContentAreaClassName?: string;
    options?: JQuerySlickOptions;
    gradientOverlay?: IGradientOverlayProps;
    store: IStore;
}

export const SlickSliderWrapper: FunctionComponent<IProps> = props =>
{
    const options = props.options || {};

    useEffect(() =>
    {
        try
        {
            $(`.${props.uniqueSliderClass}`).slick(options);
        }
        catch (e)
        {
            console.log("error initializing slider");
        }

        return () => $(`.${props.uniqueSliderClass}`).slick("unslick");
    }, []);

    options.prevArrow = `.${props.uniqueSliderClass}-prevButton`;
    options.nextArrow = `.${props.uniqueSliderClass}-nextButton`;
    options.appendDots = `.${props.uniqueSliderClass}-dotsWrapper`;
    options.customPaging = () => "<div class='dot'></div>";
    options.dotsClass = styles.dots;

    return (
        <div className={[styles.slickSliderWrapper, props.sliderClassName ? props.sliderClassName : null].join(" ")}>
            {props.gradientOverlay
                && <GradientOverlay
                    colorFrom={props.gradientOverlay.colorFrom}
                    colorTo={props.gradientOverlay.colorTo}
                    rightAndLeftSideWidth={props.gradientOverlay?.rightAndLeftSideWidth}
                />
            }
            <SliderArrow
                translateY={props.arrowsTranslateY}
                margin={props.arrowsMargin}
                type={eArrowType.prev}
                className={`${props.uniqueSliderClass}-prevButton`}
                store={props.store}
            />
            <SliderArrow
                translateY={props.arrowsTranslateY}
                margin={props.arrowsMargin}
                type={eArrowType.next}
                className={`${props.uniqueSliderClass}-nextButton`}
                store={props.store}
            />
            <div className={[props.sliderContentAreaClassName ? props.sliderContentAreaClassName : null, props.uniqueSliderClass].join(" ")}>
                {props.children}
            </div>
            <div
                style={{ transform: props.dotsTranslateY ? `translateY(${props.dotsTranslateY}px)` : undefined }}
                className={[`${props.uniqueSliderClass}-dotsWrapper`, styles.dotsWrapper].join(" ")}
            />
        </div>
    );
};

if(typeof window !== "undefined")
{
    // jquery passive event listeners

    /*    $.event.special.touchmove = {
        setup(_, ns, handle)
        {
            if(ns.includes("noPreventDefault"))
            {
                // @ts-ignore
                this.addEventListener("touchmove", handle, { passive: false });
            }
            else
            {
                // @ts-ignore
                this.addEventListener("touchmove", handle, { passive: true });
            }
        }
    };*/

    $.event.special.touchstart = {
        setup(_, ns, handle)
        {
            if(ns.includes("noPreventDefault"))
            {
                // @ts-ignore
                this.addEventListener("touchstart", handle, { passive: false });
            }
            else
            {
                // @ts-ignore
                this.addEventListener("touchstart", handle, { passive: true });
            }
        }
    };
}
