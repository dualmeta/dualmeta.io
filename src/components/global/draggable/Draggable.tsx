import React, { FunctionComponent, useEffect, useRef } from "react";

import { DraggableSlide } from "./draggableSlide/DraggableSlide";

const styles = require("./Draggable.module.sass");

interface IProps
{
    className?: string;
}

export const Draggable: FunctionComponent<IProps> = props =>
{
    let isMousePressed = false;
    let startX = null;
    let scrollLeft = null;

    const sliderRef: React.MutableRefObject<HTMLDivElement> = useRef(null);

    const startScrollAnimation = (): void =>
    {
        /* ======== CHANGE THESE VALUE TO ADJUST CURVE ======== */
        const animationDurationInFrames = 100;
        const maxSliderSpeedPerFrame = 2.5;
        /* ======== ADJUST END ======== */

        /* ======== DO NOT TOUCH ======== */
        let isAnimationRunning = false;
        let frameCounter = 0;
        const a = maxSliderSpeedPerFrame / 2;
        const b = (2 * Math.PI) / animationDurationInFrames;
        const c = animationDurationInFrames * 0.75;
        const d = maxSliderSpeedPerFrame / 2;
        /* ======== DO NOT TOUCH END ======== */

        // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
        // console.log("f(x) = " + a + " * sin(" + b + " * (" + c + " - x)) + " + d);

        const f = (x: number): number => a * Math.sin(b * (c - x)) + d;

        let calculatedAnimationScrollDistance = 0;

        for(let j = 0; j < animationDurationInFrames; j++)
        {
            calculatedAnimationScrollDistance += f(j);
        }

        const sliderOverflowAmount = sliderRef.current.scrollWidth - sliderRef.current.clientWidth;

        if(sliderOverflowAmount <= calculatedAnimationScrollDistance)
        {
            // slider overflow is not big enough. dont perform animation.
            return;
        }

        const scroll = (): void =>
        {
            // falls wer während des slides die page wechselt
            /* if(!sliderRef.current.scrollLeft)
            {
                return;
            }*/

            const currentSpeed = f(frameCounter);
            sliderRef.current.scrollLeft += currentSpeed;
            frameCounter++;

            if(currentSpeed === 0)
            {
                if(isAnimationRunning)
                {
                    return;
                }
                isAnimationRunning = true;  // --> will start in next frame
            }
            requestAnimationFrame(scroll);
        };
        requestAnimationFrame(scroll);
    };

    const onScrollHandler = (): void =>
    {
        if(sliderRef.current.getBoundingClientRect().top /* + sliderRef.current.clientHeight*/ < window.innerHeight)
        {
            startScrollAnimation();
            return window.removeEventListener("scroll", onScrollHandler);
        }
    };

    useEffect(() =>
    {
        window.addEventListener("scroll", onScrollHandler, { passive: true });
        return () => window.removeEventListener("scroll", onScrollHandler);
    }, []);

    const dragStart = (e: React.MouseEvent<HTMLDivElement, MouseEvent>): void =>
    {
        isMousePressed = true;
        sliderRef.current.style.cursor = "grabbing";
        startX = e.pageX - sliderRef.current.offsetLeft;
        ({ scrollLeft } = sliderRef.current);
    };

    const dragEnd = (): void =>
    {
        isMousePressed = false;
        sliderRef.current.style.cursor = "grab";
    };

    const onDrag = (e: React.MouseEvent<HTMLDivElement, MouseEvent>): void =>
    {
        e.preventDefault();

        if(isMousePressed)
        {
            const x = e.pageX - sliderRef.current.offsetLeft;
            const walk = (x - startX) * 1.5;  // multiply this value with a number for faster scrolling
            sliderRef.current.scrollLeft = scrollLeft - walk;
        }
    };

    const onMouseLeave = (): void =>
    {
        isMousePressed = false;
    };

    const slides = React.Children.map(props.children, (child, index) => (
        <DraggableSlide key={index}>
            {child}
        </DraggableSlide>)
    );

    return (
        <div className={[styles.outerWrapper, props.className ? props.className : null].join(" ")}>
            <div
                ref={sliderRef}
                onMouseDown={dragStart}
                onMouseMove={onDrag}
                onMouseUp={dragEnd}
                onMouseLeave={onMouseLeave}
                className={styles.wrapper}>
                <div className={styles.overlay}/>
                <div className={[styles.overlay, styles.overlayRight].join(" ")}/>
                <div className={styles.slidesWrapper}>
                    {slides}
                </div>
            </div>
        </div>
    );
};
