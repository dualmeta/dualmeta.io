import React, { FunctionComponent } from "react";

const styles = require("./DraggableSlide.module.sass");

export const DraggableSlide: FunctionComponent = props => (
    <div className={styles.slide}>
        {props.children}
    </div>
);
