import React, { useEffect, useRef } from "react";

interface IProps extends React.ComponentProps<"div">
{
    action: () => void;
    className?: string;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const useOutsideAlerter = (ref: React.RefObject<any>, action: () => void): void =>
{
    const handleClickOutside = (e: MouseEvent): void =>
    {
        if(ref.current && !ref.current.contains(e.target))
        {
            action();
        }
    };

    useEffect(() =>
    {
        document.addEventListener("mousedown", handleClickOutside, { passive: true });

        return () =>
        {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);
};

export const ClickOutsideEventDetection: React.FunctionComponent<IProps> = props =>
{
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef, props.action);

    const newProps = { ...props, action: undefined };

    return (
        <div
            {...newProps}
            onClick={props.onClick ? props.onClick : undefined}
            className={props.className ? props.className : null}
            ref={wrapperRef}>
            {props.children}
        </div>
    );
};
