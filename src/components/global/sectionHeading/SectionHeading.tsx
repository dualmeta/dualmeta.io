import React, { FunctionComponent, useEffect, useState } from "react";

const styles = require("./SectionHeading.module.sass");

export enum eHeadingType { verticalLine, horizontalLine}

interface IResponsiveOptions
{
    breakpoint: number;
    responsiveText: React.ReactElement;
}

export interface ISubHeadline
{
    text: React.ReactElement;
    responsive?: IResponsiveOptions;
}

interface IProps
{
    headline: string;
    subHeadline: ISubHeadline;
    type: eHeadingType;
    className?: string;
}

export const SectionHeading: FunctionComponent<IProps> = props =>
{
    const [windowWidth, setWindowWidth] = useState<number>(0);

    const updateWindowWidth = (): void => setWindowWidth(window.innerWidth);

    useEffect(() =>
    {
        if(props.subHeadline.responsive)
        {
            updateWindowWidth();
            window.addEventListener("resize", updateWindowWidth, { passive: true });
        }

        return () =>
        {
            if(props.subHeadline.responsive)
            {
                window.removeEventListener("resize", updateWindowWidth);
            }
        };
    }, []);

    const { responsive } = props.subHeadline;
    let subHeadline: React.ReactElement;

    if(responsive)
    {
        if(windowWidth > responsive.breakpoint)
        {
            subHeadline = props.subHeadline.text;
        }
        else
        {
            subHeadline = responsive.responsiveText;
        }
    }
    else
    {
        subHeadline = props.subHeadline.text;
    }

    let isSubHeadlineSizeReduced = false;

    // subHeadline has more than 25 characters
    if(typeof subHeadline.props.children === "object"
        && subHeadline.props.children[subHeadline.props.children.length - 1].length > 25
        && windowWidth > responsive.breakpoint)
    {
        isSubHeadlineSizeReduced = true;
    }

    return (
        <div className={[
            props.className ? props.className : null,
            props.type === eHeadingType.horizontalLine ? styles.wrapperHorizontal : styles.wrapperVertical
        ].join(" ")}>
            <div className={props.type === eHeadingType.horizontalLine ? styles.lineHorizontal : styles.lineVertical}/>
            <div className={props.type === eHeadingType.verticalLine ? styles.headlinesWrapper : null}>
                <h1 className={styles.headline}>{props.headline}</h1>
                <h2 className={[styles.subHeadline, isSubHeadlineSizeReduced ? styles.subHeadlineSmaller : null].join(" ")}>
                    {subHeadline.props.children}
                </h2>
            </div>
        </div>
    );
};
