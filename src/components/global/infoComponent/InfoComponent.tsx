import React, { FunctionComponent, useState } from "react";

import cursorActions from "../../../actions/cursorActions";
import { IStore } from "../../../utils/store";
import { ClickOutsideEventDetection } from "../clickOutsideEventDetection/ClickOutsideEventDetection";
import { FadeIn } from "../fadeIn/FadeIn";
import { Icon } from "../icon/Icon";

const styles = require("./InfoComponent.module.sass");

interface IProps
{
    icon: string;
    className?: string;
    store?: IStore;
}

enum eClickInitiator { icon, content }

export const InfoComponent: FunctionComponent<IProps> = props =>
{
    const [isExpanded, setIsExpanded] = useState<boolean>(false);
    const [isIconHovered, setIsIconHovered] = useState<boolean>(false);
    const [hasClosed, setHasClosed] = useState<boolean>(false);

    const onClickHandler = (clickInitiator: eClickInitiator): void =>
    {
        if(!isExpanded)
        {
            setIsExpanded(true);
            setHasClosed(false);
        }
        else
        {
            if(clickInitiator === eClickInitiator.icon)
            {
                setIsExpanded(false);
                setHasClosed(true);
                cursorActions.unHoverCursors(props.store);
            }
        }
    };

    let icon;

    if(isIconHovered && !isExpanded && !hasClosed)
    {
        icon = (
            <FadeIn fadeInDelay={0} fadeInSpeed={300} className={[styles.iconWrapper, styles.iconWrapper2].join(" ")}>
                <Icon
                    icon={isExpanded ? "close" : "arrow_back_ios"}
                    style={{ fontSize: isExpanded ? 24 : undefined }}
                    className={[styles.icon].join(" ")}
                />
            </FadeIn>
        );
    }
    else
    {
        icon = <Icon
            icon={isExpanded ? "close" : props.icon}
            style={{ fontSize: isExpanded ? 24 : undefined }}
            className={[styles.icon, isIconHovered ? styles.iconHover : null].join(" ")}
        />;
    }

    return (
        <div className={[styles.outerWrapper, props.className ? props.className : null].join(" ")}
            onMouseEnter={ isExpanded ? () => cursorActions.hoverCursors(props.store) : null }
            onMouseLeave={ isExpanded ? () => cursorActions.unHoverCursors(props.store) : null}>
            <ClickOutsideEventDetection
                className={[styles.wrapper, isExpanded ? styles.wrapperActive : null].join(" ")}
                action={() => { setIsExpanded(false); setHasClosed(true); }}>
                <div
                    className={[styles.iconWrapper, isExpanded ? styles.iconWrapperActive : null].join(" ")}
                    onClick={() => onClickHandler(eClickInitiator.icon)}
                    onMouseEnter={() => { setIsIconHovered(true); setHasClosed(false); if(!isExpanded) { cursorActions.hoverCursors(props.store); } }}
                    onMouseLeave={() => { setIsIconHovered(false); if(!isExpanded) { cursorActions.unHoverCursors(props.store); } }}>
                    {icon}
                </div>
                <div className={styles.contentWrapper} onClick={() => onClickHandler(eClickInitiator.content)}>
                    {props.children}
                </div>
            </ClickOutsideEventDetection>
        </div>
    );
};
