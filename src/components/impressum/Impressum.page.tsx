import React, { Fragment } from "react";

import { IStore, IStoreProps } from "../../utils/store";
import { Cursor } from "../global/cursor/Cursor";
import { Footer } from "../global/footer/Footer";
import { Header } from "../global/header/Header";
import { HeadingWithUnderline } from "../global/headingWithUnderline/HeadingWithUnderline";

const styles = require("../Imprint.module.sass");

export class ImpressumPage extends React.Component<IStoreProps, IStoreProps>
{
    public constructor(props: IStoreProps)
    {
        super(props);

        this.state = {
            store: props.store
        };

        props.store.subscribers.push((updatedStore: IStore) =>
        {
            this.setState({ store: updatedStore });
        });
    }

    public render(): React.ReactElement
    {
        const { store } = this.state;

        return (
            <Fragment>
                <Cursor store={store}/>
                <Header store={store}/>
                <div className={styles.contentWrapper}>
                    <HeadingWithUnderline text={"Impressum"}/>
                    <Fragment>
                        <h2>Angaben gemäß § 5 TMG</h2>
                        <p>Dualmeta GmbH<br/>
                            Grabenstraße 35<br/>
                            73033 Göppingen<br/>
                            Deutschland<br/>
                            <br/>
                            Telefon: 017641506151<br/>
                            E-Mail: hello[at]dualmeta.io
                            <br/>
                            <br/>
                            Registergericht: Amtsgericht Ulm
                            <br/>
                            Registernummer: HRB 743566
                            <br/>
                            <br/>
                            Geschäftsführer: Tobias Auwärter, Christian Kottmann
                        </p>

                        <h2>Streitschlichtung</h2>
                        <p>Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit: <a
                            href="https://ec.europa.eu/consumers/odr" target="_blank"
                            rel="noopener noreferrer">https://ec.europa.eu/consumers/odr</a>.<br/> Unsere E-Mail-Adresse
                            finden Sie oben im Impressum.</p>

                        <p>Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer
                            Verbraucherschlichtungsstelle teilzunehmen.</p>

                        <h2>Haftung für Inhalte</h2> <p>Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte
                        auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als
                        Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu
                        überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.</p>
                        <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen
                            Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt
                            der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden
                            Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p> <h2>Haftung für Links</h2>
                        <p>Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss
                            haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die
                            Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten
                            verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche
                            Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht
                            erkennbar.</p> <p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne
                        konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von
                        Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p> <h2>Urheberrecht</h2> <p>Die
                        durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen
                        Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb
                        der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw.
                        Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen
                        Gebrauch gestattet.</p> <p>Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden,
                        werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche
                        gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir
                        um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige
                        Inhalte umgehend entfernen.</p>

                    </Fragment>
                </div>
                <Footer store={store}/>
            </Fragment>
        );
    }
}
