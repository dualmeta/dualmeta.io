import * as React from "react";
import { FunctionComponent, useRef, useEffect } from "react";

import cursorActions from "../../../actions/cursorActions";
import refActions from "../../../actions/refActions";
import { EScrollPoint } from "../../../utils/enums";
import { IClassProps } from "../../../utils/interfaces";
import { IStoreProps } from "../../../utils/store";
import { ContentBox } from "../contentBox/ContentBox";
import { Rezensionen } from "./rezensionen/Rezensionen";

const sliderArrowStyles = require("../../global/slickSliderWrapper/sliderArrow/SliderArrow.module.sass");

interface IProps extends IClassProps, IStoreProps {}

export const RezensionenWrapper: FunctionComponent<IProps> = props =>
{
    const ref = useRef(null);

    useEffect(() =>
    {
        refActions.addRef(props.store, ref);
    }, []);

    return (
        <div ref={ref} id={EScrollPoint.rezensionen}>
            <ContentBox
                store={props.store}
                className={props?.className}
                headline={"Rezensionen"}
                subHeadline={{
                    text: <p>Weitere Bewertungen finden Sie auf <a
                        href={"https://www.google.com/search?q=dualmeta&rlz=1C1CHBF_deDE986DE986&oq=dualmeta+&aqs=chrome.0.69i59l2j69i60l2j69i61j69i65.2808j0j7&sourceid=chrome&ie=UTF-8#lrd=0x4799a7a21a931cf7:0xf173e0f9ca7f4996,1,,,"}
                        onMouseEnter={() => cursorActions.hoverCursors(props.store)}
                        onMouseLeave={() => cursorActions.unHoverCursors(props.store)}
                        target={"blank"}>Google</a></p>
                }}
                ctaBottom={{
                    text: "Jetzt Kontakt aufnehmen",
                    hideAt: parseInt(sliderArrowStyles.hideArrowsAt),
                    scrollPoint: EScrollPoint.kontakt
                }}>
                <Rezensionen store={props.store}/>
            </ContentBox>
        </div>
    );
};
