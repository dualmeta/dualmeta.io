import React, { FunctionComponent } from "react";

import { IStoreProps } from "../../../../utils/store";
import { SlickSliderWrapper } from "../../../global/slickSliderWrapper/SlickSliderWrapper";
import { rezensionen } from "./data";
import { RezensionSlide } from "./rezensionSlide/RezensionSlide";

const sliderArrowStyles = require("../../../global/slickSliderWrapper/sliderArrow/SliderArrow.module.sass");
const styles = require("./Rezensionen.module.sass");

export const Rezensionen: FunctionComponent<IStoreProps> = props => (
    <SlickSliderWrapper
        store={props.store}
        sliderContentAreaClassName={styles.rezensionenWrapper}
        dotsTranslateY={0}
        options={{
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: parseInt(sliderArrowStyles.hideArrowsAt),
                    settings: {
                        dots: true,
                        slidesToShow: 1
                    }
                }
            ]
        }}
        uniqueSliderClass={"rezensionen-slider-wrapper"}>
        {rezensionen.map(({
            id,
            img,
            imgWebP,
            clientName,
            clientCompany,
            stars,
            text
        }) => (
            <RezensionSlide
                key={id}
                img={img}
                imgWebP={imgWebP}
                clientName={clientName}
                clientCompany={clientCompany}
                stars={stars}>
                {text}
            </RezensionSlide>
        ))}
    </SlickSliderWrapper>
);
