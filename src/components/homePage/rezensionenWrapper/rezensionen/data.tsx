import React from "react";

import d from "../../../../assets/rezensionen/jpg/David.jpg";
import g from "../../../../assets/rezensionen/jpg/Giulio.jpg";
import l from "../../../../assets/rezensionen/jpg/Luca.jpg";
import m from "../../../../assets/rezensionen/jpg/michael.jpg";
import n from "../../../../assets/rezensionen/jpg/Nico.jpg";
import p from "../../../../assets/rezensionen/jpg/Phil.jpg";
import r from "../../../../assets/rezensionen/jpg/Raphaela.jpg";
import ro from "../../../../assets/rezensionen/jpg/Rocco.jpg";
import t from "../../../../assets/rezensionen/jpg/Thomas.jpg";
import v from "../../../../assets/rezensionen/jpg/Vivien.jpg";
import Foerl from "../../../../assets/rezensionen/michael-foerl.jpg";
import FoerlWebP from "../../../../assets/rezensionen/michael-foerl.webp";
import Philipp from "../../../../assets/rezensionen/philipp.jpg";
import PhilippWebP from "../../../../assets/rezensionen/philipp.webp";
import dp from "../../../../assets/rezensionen/webp/David.webp";
import gp from "../../../../assets/rezensionen/webp/Giulio.webp";
import lp from "../../../../assets/rezensionen/webp/Luca.webp";
import mP from "../../../../assets/rezensionen/webp/michael.webp";
import np from "../../../../assets/rezensionen/webp/Nico.webp";
import pp from "../../../../assets/rezensionen/webp/Phil.webp";
import rp from "../../../../assets/rezensionen/webp/Raphaela.webp";
import rop from "../../../../assets/rezensionen/webp/Rocco.webp";
import tp from "../../../../assets/rezensionen/webp/Thomas.webp";
import vp from "../../../../assets/rezensionen/webp/Vivien.webp";
import { IProps as IRezensionSlideProps } from "./rezensionSlide/RezensionSlide";

export interface IRezension extends IRezensionSlideProps
{
    id: number;
    text: React.ReactElement;
}

export const rezensionen: IRezension[] = [
    {
        id: 1,
        img: v,
        imgWebP: vp,
        clientName: "Vivien",
        clientCompany: "Combitherm GmbH",
        stars: 10,
        text: (
            <p>
              Mit der Zusammenarbeit und Neugestaltung unserer Website durch Dualmeta sind wir mehr als zufrieden!
              Die neue Website sieht sehr professionell aus und ist dank der guten Einführung einfach und intuitiv
              zu verwalten. Die Kompetenz der beiden ist auf einem wirklich sehr hohen Niveau, daher wärmstens
              weiterzuempfehlen!
            </p>
        )
    },
    {
        id: 2,
        img: n,
        imgWebP: np,
        clientName: "Nico",
        clientCompany: "Make Opinion GmbH",
        stars: 10,
        text: (
            <p>
                Wir arbeiten jetzt bereits knapp ein Jahr mit Tobi von Dualmeta zusammen. Die Kooperation lief reibungslos. Tobias hat uns bei diversen Produkten betreut, die nicht von der Stange sind. Alle Projekte wurden schnell verstanden und umgesetzt.
                Wir mögen vor allen das eigenständige arbeiten und DENKEN was heute leider nicht mehr selbstverständlich ist.  Wir sehen uns beim nächsten Project.
            </p>
        )
    },
    {
        id: 3,
        img: d,
        imgWebP: dp,
        clientName: "David",
        clientCompany: "Peter Sauber Agentur",
        stars: 10,
        text: (
            <p>
                A big thank you to the team at Dualmeta for their excellent work for us at Peter Sauber Agentur! They examined our issue (a WordPress website with two databases and multiple plugins) and quickly created and implemented a (JavaScript and PHP) solution that was simple, convenient and cost effective. As an events company with tight deadlines and many moving parts, we were very grateful to have had Dualmeta at our side for our “f-cell Stuttgart” event. Highly recommended!
            </p>
        )
    },
    {
        id: 4,
        img: t,
        imgWebP: tp,
        clientName: "Thomas",
        clientCompany: "VEKO GmbH",
        stars: 10,
        text: (
            <p>
                Sehr freundliche und äußerst kompetente Beratung. Perfekte Umsetzung der besprochenen Inhalte in einem ersten Konzeptvorschlag. Super Gestaltung und Erstellung der Website nach unseren Vorstellungen. Service und Support äußerst problemlos und zeitnah. Immer wieder sehr gerne, nur zu empfehlen! Junges Unternehmen mit potential.
            </p>
        )
    },
    {
        id: 5,
        img: l,
        imgWebP: lp,
        clientName: "Luca",
        clientCompany: "Fundis Reitsport",
        stars: 10,
        text: (
            <p>
                Die Zusammenarbeit an Kundenprojekten mit Dualmeta als selbstständiger Webentwickler funktioniert perfekt. Beide Entwickler arbeiten agil, effizient und programmieren auf hohem Niveau.
            </p>
        )
    },
    {
        id: 6,
        img: m,
        imgWebP: mP,
        clientName: "Dr. Michael Förl",
        clientCompany: "Augenärzte Förl - Göppingen",
        stars: 10,
        text: (
            <p>
              Vielen Dank an das Team von Dualmeta für die Neugestaltung meiner Praxishomepage. Die neue Webseite sieht sehr professionell aus und ist auch für mich als Laien, dank der detaillierten Anleitung, einfach auf dem Laufenden zu halten. Ich habe von meinen Patienten schon sehr positives Feedback erhalten! Weiter so.
            </p>
        )
    },
    {
        id: 7,
        img: ro,
        imgWebP: rop,
        clientName: "Rocco",
        clientCompany: "Flowcess",
        stars: 10,
        text: (
            <p>
                Ich habe das Team von Dualmeta (Christian und Tobias) über das Startup Center der Hochschule der Medien kennengelernt.
                Die beiden sind wirklich engagiert, super strukturiert und haben uns sehr weitergeholfen. Die Kommunikation war klasse und ihr Arbeitsweise sehr professionell.
                Die Jungs echt cool drauf und stehen mir bei all meinen technischen Fragen zur Seite.
                Also ganz klare Weiterempfehlung!
            </p>
        )
    },
    {
        id: 8,
        img: p,
        imgWebP: pp,
        clientName: "Phil",
        clientCompany: "hitcap esports GmbH",
        stars: 10,
        text: (
            <p>
                Wurden von einem Kollegen wärmstens empfohlen und übertreffen an Kompetenz und Zuverlässigkeit jegliche Erwartungen. Zusätzlich bringen sie sich stets ein, haben die übergeordneten Projektziele im Blick und tragen maßgeblich zum Erfolg des Projekts, nicht zuletzt durch ihre konzeptionelle Fähigkeiten, bei.
            </p>
        )
    },
    {
        id: 9,
        img: r,
        imgWebP: rp,
        clientName: "Raphaela",
        clientCompany: "Help for Children",
        stars: 10,
        text: (
            <p>
              Meine neu erstellte Homepage wurde ganz nach meinen Vorstellungen perfekt von den Dualmeta-Jungs umgesetzt. Ich bin mit dem Endergebnis sehr zufrieden. Die Jungs von Dualmeta sind sehr zuverlässig, schnell und perfektionistisch, genau nach meinem Geschmack. Auch wurde ich super einfach und schnell in meine neue Homepage eingelernt. Nun kann ich auch als Laie kleine Veränderungen an meiner neuen Homepage tätigen. Vielen lieben Dank für die gute Zusammenarbeit.
            </p>
        )
    },
    {
        id: 10,
        img: g,
        imgWebP: gp,
        clientName: "Giulio",
        clientCompany: "HKH Online Marketing UG",
        stars: 10,
        text: (
            <p>
                Ich bin durch einen Bekannten auf Dualmeta aufmerksam geworden. Eine neue Website für uns wurde genau nach unseren Vorstellungen umgesetzt und mit dem Ergebnis sind wir mehr als zufrieden. Wer Qualität will ist hier also absoulut richtig.
            </p>
        )
    },
];
