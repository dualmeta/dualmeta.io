import React, { FunctionComponent, ReactElement } from "react";

const styles = require("./RezensionSlide.module.sass");

export interface IProps
{
    img: string;
    imgWebP: string;
    clientName: string;
    clientCompany: string;
    stars: number;
}

const buildStars = (starsAmount: number): ReactElement[] =>
{
    const stars: React.ReactElement[] = [];

    const fullStars = Math.floor(starsAmount / 2);
    const halfStars = starsAmount - 2 * fullStars;
    const emptyStarts = Math.floor((10 - starsAmount) / 2);

    // needed to generate a key for each star
    let j = 0;

    for(let i = 0; i < fullStars; i++)
    {
        stars.push(
            <svg key={j} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"/>
                <path d="M0 0h24v24H0z" fill="none"/>
            </svg>
        );
        j++;
    }

    for(let i = 0; i < halfStars; i++)
    {
        stars.push(
            <svg key={j} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="none" d="M0 0h24v24H0z"/>
                <path d="M19.65 9.04l-4.84-.42-1.89-4.45c-.34-.81-1.5-.81-1.84 0L9.19 8.63l-4.83.41c-.88.07-1.24 1.17-.57 1.75l3.67 3.18-1.1 4.72c-.2.86.73 1.54 1.49 1.08l4.15-2.5 4.15 2.51c.76.46 1.69-.22 1.49-1.08l-1.1-4.73 3.67-3.18c.67-.58.32-1.68-.56-1.75zM12 15.4V6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"/>
            </svg>
        );
        j++;
    }

    for(let i = 0; i < emptyStarts; i++)
    {
        stars.push(
            <svg key={j} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="none" d="M0 0h24v24H0V0z"/>
                <path d="M19.65 9.04l-4.84-.42-1.89-4.45c-.34-.81-1.5-.81-1.84 0L9.19 8.63l-4.83.41c-.88.07-1.24 1.17-.57 1.75l3.67 3.18-1.1 4.72c-.2.86.73 1.54 1.49 1.08l4.15-2.5 4.15 2.51c.76.46 1.69-.22 1.49-1.08l-1.1-4.73 3.67-3.18c.67-.58.32-1.68-.56-1.75zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"/>
            </svg>
        );
        j++;
    }

    return stars;
};

export const RezensionSlide: FunctionComponent<IProps> = props => (
    <div className={styles.slideWrapper}>
        <div className={styles.innerBox}>
            <div className={styles.imgWrapper}>
                <picture>
                    <source srcSet={props.imgWebP} type="image/webp"/>
                    <source srcSet={props.img} type="image/jpg"/>
                    <img src={props.img} alt="Referenz Bild"/>
                </picture>
            </div>
            <h3>{props.clientName}</h3>
            <h4>{props.clientCompany}</h4>
            <div className={styles.starsWrapper}>
                {buildStars(props.stars)}
            </div>
            {props.children}
        </div>
    </div>
);

