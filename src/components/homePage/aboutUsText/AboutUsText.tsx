import React, { FunctionComponent } from "react";

import { IClassProps } from "../../../utils/interfaces";

const styles = require("./AboutUsText.module.sass");

export const AboutUsText: FunctionComponent<IClassProps> = props => (
    <div className={[styles.wrapper, props?.className].join(" ")}>
        <p className={styles.text} lang={"de"}>
          Wir sind eine aufstrebende <b>Webagentur</b> aus dem Raum <b>Stuttgart</b> und haben uns auf die Entwicklung von <b>Websites und Webanwendungen</b> spezialisiert. Wir lieben <b>sauberen Code</b>, legen großen Wert auf <b>Zuverlässigkeit</b> und sind hoch motiviert, unseren Kunden das <b>bestmögliche Endprodukt</b> zu liefern.
        </p>
    </div>
);
