import React, { FunctionComponent } from "react";

import BackgroundImage from "../../../assets/landing/bg_minified.png";
import { FadeIn } from "../../global/fadeIn/FadeIn";
import { TypedText } from "../../global/typedText/TypedText";

const styles = require("./LandingPage.module.sass");

export const LandingPage: FunctionComponent = () => (
    <div className={styles.wrapper}>
        <div className={styles.headingWrapper}>
            <TypedText typedText={"We deliver what you expect."} className={styles.typedText}/>
            <FadeIn fadeInDelay={1700}>
                <h1 className={styles.subTitle}>Webentwicklung <span className={styles.subTitleDivider}>|</span> Webdesign <span className={styles.subTitleDivider}>|</span> Appentwicklung </h1>
                <h1 className={[styles.subTitle, styles.subTitleResponsive].join(" ")}>Webentwicklung aus Stuttgart</h1>
            </FadeIn>
        </div>
        <FadeIn className={styles.fadeIn} fadeInDelay={0} fadeInSpeed={1500}>
            <div className={styles.bgImageOverlay}/>
            <div
                className={styles.bgImageWrapper}
                style={{ backgroundImage: "url('" + BackgroundImage + "')" }}
            />
        </FadeIn>
        <div className={styles.transitionTop}/>
        <div className={styles.transitionBottom}/>
    </div>
);
