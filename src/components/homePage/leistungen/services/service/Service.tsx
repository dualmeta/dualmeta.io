import React, { FunctionComponent } from "react";

import { Icon } from "../../../../global/icon/Icon";

const styles = require("./Service.module.sass");

interface IProps
{
    icon: string;
    heading: string;
}

export const Service: FunctionComponent<IProps> = props => (
    <div className={styles.serviceWrapper}>
        <Icon className={styles.icon} icon={props.icon}/>
        <h3 className={styles.heading}>{props.heading}</h3>
        {props.children}
    </div>
);

