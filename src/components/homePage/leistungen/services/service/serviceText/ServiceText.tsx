import React, { FunctionComponent } from "react";

const styles = require("./ServiceText.module.sass");

export const ServiceText: FunctionComponent = props => (
    <p className={styles.text} lang={"de"}>
        {props.children}
    </p>
);

