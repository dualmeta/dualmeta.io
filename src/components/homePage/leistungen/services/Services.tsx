import React, { FunctionComponent } from "react";

import { Service } from "./service/Service";
import { ServiceText } from "./service/serviceText/ServiceText";

const styles = require("./Services.module.sass");

export const Services: FunctionComponent = () => (
    <div className={styles.wrapper}>
        <Service icon={"code"} heading={"Webentwicklung"}>
            <ServiceText>
                Wir ent&shy;wick&shy;eln in&shy;no&shy;va&shy;ti&shy;ve Full Stack Web&shy;an&shy;wen&shy;dung&shy;en
                und digi&shy;tal&shy;isie&shy;ren Ihre Pro&shy;zes&shy;se. Da&shy;bei set&shy;zen wir auf mo&shy;derns&shy;te
                Frame&shy;works. Ganz egal ob on&shy;line Ter&shy;min&shy;pla&shy;ner, Vi&shy;de&shy;o&shy;platt&shy;form
                oder ein ei&shy;gen&shy;es Pro&shy;jekt&shy;mana&shy;gement Tool&nbsp;- Wir ma&shy;chen es mög&shy;lich.
            </ServiceText>
        </Service>
        <Service icon={"phonelink"} heading={"Webdesign"}>
            <ServiceText>
              Unsere Websites sind hochwertig, modern und fernab von Standard.
              Wir designen und entwickeln Websites, die sich hinsichtlich Design und Funktionalität deutlich von der
              Konkurrenz abheben und Ihr Unternehmen oder Dienstleistung zum Besten Ihrer Branche machen.
            </ServiceText>
        </Service>
        <Service icon={"phone_iphone"} heading={"Appentwicklung"}>
            <ServiceText>
          Sie be&shy;nö&shy;ti&shy;gen einen ko&shy;mpe&shy;ten&shy;ten Part&shy;ner, der mit Ih&shy;nen ge&shy;mein&shy;sam
          die Um&shy;setz&shy;ung einer App in An&shy;griff nimmt? Wir be&shy;ra&shy;ten Sie bei der Wahl der
          Tech&shy;no&shy;lo&shy;gie, un&shy;ter&shy;stüt&shy;zen sie beim De&shy;sign und ü&shy;ber&shy;neh&shy;men
          die kom&shy;plet&shy;te Ent&shy;wick&shy;lung der App in&shy;klu&shy;sive Ser&shy;ver und
          Da&shy;ten&shy;bank&shy;an&shy;bin&shy;dung.
            </ServiceText>
        </Service>
    </div>
);

