import * as React from "react";
import { FunctionComponent, useRef, useEffect } from "react";

import refActions from "../../../actions/refActions";
import { EScrollPoint } from "../../../utils/enums";
import { IClassProps } from "../../../utils/interfaces";
import { IStoreProps } from "../../../utils/store";
import { ContentBox } from "../contentBox/ContentBox";
import { Services } from "./services/Services";

interface IProps extends IClassProps, IStoreProps {}

export const Leistungen: FunctionComponent<IProps> = props =>
{
    const ref = useRef(null);

    useEffect(() =>
    {
        refActions.addRef(props.store, ref);
    }, []);

    return (
        <div ref={ref} id={EScrollPoint.leistungen}>
            <ContentBox
                store={props.store}
                className={props?.className}
                headline={"Leistungen"}
                subHeadline={{ text: <p>Digitalisierung durch innovative Webentwicklung</p> }}
                ctaTopRight={{ text: "Jetzt Kontakt aufnehmen", responsive: { text: "Kontakt", breakpoint: 1200 } }}
                ctaBottom={{ text: "Mehr erfahren", scrollPoint: EScrollPoint.warumDualmeta }}>
                <Services/>
            </ContentBox>
        </div>
    );
};
