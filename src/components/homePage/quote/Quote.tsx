import React, { FunctionComponent } from "react";

import { IClassProps } from "../../../utils/interfaces";
import { Icon } from "../../global/icon/Icon";

const styles = require("./Quote.module.sass");

interface IProps extends IClassProps
{
    text: string;
    author: string;
}

export const Quote: FunctionComponent<IProps> = props => (
    <div className={[styles.wrapper, props?.className].join(" ")}>
        <Icon icon={"format_quote"} className={styles.icon}/>
        <p className={styles.quote}>&quot;{props.text}&quot;</p>
        <p className={styles.author}>{props.author}</p>
    </div>
);
