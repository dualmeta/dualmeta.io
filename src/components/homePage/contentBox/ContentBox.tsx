import React, { FunctionComponent } from "react";

import refActions from "../../../actions/refActions";
import { EScrollPoint } from "../../../utils/enums";
import { IStoreProps } from "../../../utils/store";
import { Button, eButtonType, IResponsiveText } from "../../global/button/Button";
import { eHeadingType, ISubHeadline, SectionHeading } from "../../global/sectionHeading/SectionHeading";

const styles = require("./ContentBox.module.sass");

interface ICtaTopRight
{
    text: string;
    responsive?: IResponsiveText;
}

interface ICtaBottom
{
    text: string;
    hideAt?: number;
    scrollPoint?: string;
}

interface IProps extends IStoreProps
{
    headline: string;
    subHeadline: ISubHeadline;
    ctaBottom?: ICtaBottom;
    ctaTopRight?: ICtaTopRight;
    className?: string;
    contentClassName?: string;
}

export const ContentBox: FunctionComponent<IProps> = props =>
{
    let ctaTopRight;
    let ctaBottom;

    if(props.ctaTopRight)
    {
        ctaTopRight = (
            <Button
                className={styles.ctaTopRight}
                type={eButtonType.outlined}
                text={props.ctaTopRight.text}
                responsiveText={props.ctaTopRight.responsive
                    && { text: props.ctaTopRight.responsive.text, breakpoint: props.ctaTopRight.responsive.breakpoint }
                }
                onClick={() => refActions.scrollToRef(props.store, EScrollPoint.kontakt)}
                store={props.store}
            />
        );
    }

    if(props.ctaBottom)
    {
        ctaBottom = (
            <Button
                hideAt={props.ctaBottom?.hideAt}
                type={eButtonType.filled}
                text={props.ctaBottom.text}
                onClick={() => props.ctaBottom.scrollPoint ? refActions.scrollToRef(props.store, props.ctaBottom.scrollPoint) : null}
                store={props.store}
            />
        );
    }

    return (
        <div className={[styles.outerWrapper, props.className ? props.className : null].join(" ")}>
            <div className={styles.contentBox}>
                <div className={styles.header}>
                    <SectionHeading headline={props.headline} subHeadline={props.subHeadline} type={eHeadingType.verticalLine}/>
                    {ctaTopRight}
                </div>
                <div className={[styles.content, props.contentClassName ? props.contentClassName : null].join(" ")}>
                    {props.children}
                </div>
                {ctaBottom}
            </div>
            <div className={styles.backdrop}/>
        </div>
    );
};
