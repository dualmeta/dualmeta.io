import * as React from "react";
import { FunctionComponent } from "react";

const styles = require("./ImgAndText.module.sass");

interface IProps
{
    image: string;
    imageWebP: string;
    text: string;
    className?: string;
}

export const ImgAndText: FunctionComponent<IProps> = props =>
    <div className={[styles.wrapper, props?.className].join(" ")}>
        <picture>
            <source srcSet={props.imageWebP} type="image/webp"/>
            <source srcSet={props.image} type="image/jpg"/>
            <img draggable={false} className={styles.img} src={props.image} alt={"Stack Logo"}/>
        </picture>
        <p className={styles.imgText}>{props.text}</p>
    </div>;
