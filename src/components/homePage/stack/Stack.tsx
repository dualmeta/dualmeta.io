import React, { FunctionComponent } from "react";

import Firebase from "../../../assets/stackLogos/firebase.png";
import FirebaseWebP from "../../../assets/stackLogos/firebase.webp";
import Flutter from "../../../assets/stackLogos/flutter.png";
import FlutterWebP from "../../../assets/stackLogos/flutter.webp";
import Mongo from "../../../assets/stackLogos/mongodb.png";
import MongoWebP from "../../../assets/stackLogos/mongodb.webp";
import Node from "../../../assets/stackLogos/node.png";
import NodeWebP from "../../../assets/stackLogos/node.webp";
import ReactLogo from "../../../assets/stackLogos/react.png";
import ReactLogoWebP from "../../../assets/stackLogos/react.webp";
import Sql from "../../../assets/stackLogos/sql.png";
import SqlWebP from "../../../assets/stackLogos/sql.webp";
import Ts from "../../../assets/stackLogos/ts.png";
import TsWebP from "../../../assets/stackLogos/ts.webp";
import Wp from "../../../assets/stackLogos/wp.png";
import WpWebP from "../../../assets/stackLogos/wp.webp";
import { IClassProps } from "../../../utils/interfaces";
import { Draggable } from "../../global/draggable/Draggable";
import { ImgAndText } from "./ImgAndText/ImgAndText";

const styles = require("./Stack.module.sass");

export const Stack: FunctionComponent<IClassProps> = props => (
    <div className={[styles.wrapper, props?.className].join(" ")}>
        <Draggable className={styles.stackLogoWrapper}>
            <ImgAndText image={ReactLogo} text={"ReactJS"} imageWebP={ReactLogoWebP}/>
            <ImgAndText image={Ts} text={"Typescript"} imageWebP={TsWebP}/>
            <ImgAndText image={Firebase} text={"firebase"} imageWebP={FirebaseWebP} className={styles.firebase}/>
            <ImgAndText image={Node} text={"Nodejs"} imageWebP={NodeWebP}/>
            <ImgAndText image={Sql} text={"sql"} imageWebP={SqlWebP}/>
            <ImgAndText image={Mongo} text={"Mongodb"} imageWebP={MongoWebP}/>
            <ImgAndText image={Wp} text={"wordpress"} imageWebP={WpWebP}/>
            <ImgAndText image={Flutter} text={"flutter"} imageWebP={FlutterWebP}/>
        </Draggable>
    </div>
);
