import * as React from "react";
import { FunctionComponent } from "react";

import { IClassProps } from "../../../utils/interfaces";
import { IStoreProps } from "../../../utils/store";
import { ContentBox } from "../contentBox/ContentBox";
import { UnsereWebprojekte } from "./unsereWebprojekte/UnsereWebprojekte";

const styles = require("./WebProjekte.module.sass");

interface IProps extends IClassProps, IStoreProps {}

export const WebProjekte: FunctionComponent<IProps> = props => (
    <ContentBox
        store={props.store}
        contentClassName={styles.rezensionenContent}
        className={props?.className}
        headline={"Websites"}
        subHeadline={{ text: <p>Unsere Kunden vermitteln auch online den bestmöglichen Eindruck</p> }}
        ctaTopRight={{ text: "Jetzt Kontakt aufnehmen", responsive: { text: "Kontakt", breakpoint: 1200 } }}>
        <UnsereWebprojekte store={props.store}/>
    </ContentBox>
);
