import React, { FunctionComponent } from "react";

import Combi from "../../../../assets/unsereWebprojekte/combi-min.png";
import CombiWebP from "../../../../assets/unsereWebprojekte/combi-min.webp";
import PraxisFoerl from "../../../../assets/unsereWebprojekte/Foerl-min.png";
import PraxisFoerlWebP from "../../../../assets/unsereWebprojekte/Foerl-min.webp";
import Friedrichs from "../../../../assets/unsereWebprojekte/Friedrichs-min.png";
import FriedrichsWebP from "../../../../assets/unsereWebprojekte/Friedrichs-min.webp";
import HfC from "../../../../assets/unsereWebprojekte/HfC-min.png";
import HfCWebP from "../../../../assets/unsereWebprojekte/HfC-min.webp";
import Modi from "../../../../assets/unsereWebprojekte/MODI-min.png";
import ModiWebP from "../../../../assets/unsereWebprojekte/MODI-min.webp";
import MSGR from "../../../../assets/unsereWebprojekte/MSGR-min.png";
import MSGRWebP from "../../../../assets/unsereWebprojekte/MSGR-min.webp";
import Stoll from "../../../../assets/unsereWebprojekte/Stoll-min.png";
import StollWebP from "../../../../assets/unsereWebprojekte/Stoll-min.webp";
import Veko from "../../../../assets/unsereWebprojekte/Veko-min.png";
import VekoWebP from "../../../../assets/unsereWebprojekte/Veko-min.webp";
import { IStoreProps } from "../../../../utils/store";
import { SlickSliderWrapper } from "../../../global/slickSliderWrapper/SlickSliderWrapper";
import { WebprojektSlide } from "./webprojektSlide/WebprojektSlide";

const styles = require("./UnsereWebprojekte.module.sass");

export const UnsereWebprojekte: FunctionComponent<IStoreProps> = props => (
    <SlickSliderWrapper
        store={props.store}
        sliderContentAreaClassName={styles.webprojekteWrapper}
        dotsTranslateY={20}
        options={{
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
            responsive: [
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }}
        uniqueSliderClass={"referenzen-slider-wrapper"}>
        <WebprojektSlide img={PraxisFoerl} imgWebP={PraxisFoerlWebP} store={props.store} link={"https://www.praxis-foerl.de/"}/>
        <WebprojektSlide img={Combi} imgWebP={CombiWebP} store={props.store} link={"https://www.combitherm.de//"}/>
        <WebprojektSlide img={Modi} imgWebP={ModiWebP} store={props.store} link={"https://modi.hdm-stuttgart.de/"}/>
        <WebprojektSlide img={HfC} imgWebP={HfCWebP} store={props.store} link={"https://www.helpforchildren.de/"}/>
        <WebprojektSlide img={Veko} imgWebP={VekoWebP} store={props.store} link={"https://www.vekogmbh.com/"}/>
        <WebprojektSlide img={MSGR} imgWebP={MSGRWebP} store={props.store} link={"https://msgr-studio.com/"}/>
        <WebprojektSlide img={Stoll} imgWebP={StollWebP} store={props.store} link={"https://www.stoll.group/"}/>
        <WebprojektSlide img={Friedrichs} imgWebP={FriedrichsWebP} store={props.store} link={"https://friedrichs-club.de/"}/>
    </SlickSliderWrapper>
);
