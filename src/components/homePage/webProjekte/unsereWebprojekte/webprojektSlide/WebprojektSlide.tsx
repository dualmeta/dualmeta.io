import React, { FunctionComponent } from "react";

import cursorActions from "../../../../../actions/cursorActions";
import { IStore } from "../../../../../utils/store";

const styles = require("./WebprojektSlide.module.sass");

interface IProps
{
    img: string;
    imgWebP: string;
    link?: string;
    store: IStore;
}

export const WebprojektSlide: FunctionComponent<IProps> = props => (
    <div className={styles.slideWrapper}>
        <div className={styles.innerBox}
            onMouseMove={() => cursorActions.hoverCursors(props.store)}
            onMouseLeave={() => cursorActions.unHoverCursors(props.store)}
            onClick={() => typeof window === "undefined" ? null : window.open(props.link)}>
            <picture>
                <source srcSet={props.imgWebP} type="image/webp"/>
                <source srcSet={props.img} type="image/jpg"/>
                <img src={props.img} alt="website bild referenz"/>
            </picture>
        </div>
    </div>
);

