import $ from "jquery";
import React, { Fragment } from "react";

import { browserActions } from "../../actions/browserActions";
import cursorActions from "../../actions/cursorActions";
import { IStore, IStoreProps } from "../../utils/store";
import { CookieBanner } from "../global/cookieBanner/CookieBanner";
import { Cursor } from "../global/cursor/Cursor";
import { Header } from "../global/header/Header";
import { AboutUsText } from "./aboutUsText/AboutUsText";
import { Kontakt } from "./kontakt/Kontakt";
import { LandingPage } from "./landingPage/LandingPage";
import { Leistungen } from "./leistungen/Leistungen";
import { LiteDesk } from "./liteDesk/LiteDesk";
import { Quote } from "./quote/Quote";
import { RezensionenWrapper } from "./rezensionenWrapper/RezensionenWrapper";
import { Stack } from "./stack/Stack";
import { WarumDualmeta } from "./warumDualmeta/WarumDualmeta";
import { WebProjekte } from "./webProjekte/WebProjekte";
import { Yafa } from "./yafa/Yafa";

const styles = require("./HomePage.module.sass");

interface IState extends IStoreProps
{
    amountOfDots: number;
}

class HomePage extends React.Component<IStoreProps, IState>
{
    public constructor(props: IStoreProps)
    {
        super(props);

        this.state = {
            store: props.store,
            amountOfDots: 0
        };

        props.store.subscribers.push((updatedStore: IStore) =>
        {
            this.setState({ store: updatedStore });
        });
    }

    public componentDidMount(): void
    {
        browserActions.checkWebPSupport(this.state.store);
    }

    public componentDidUpdate(prevProps: Readonly<IStoreProps>, prevState: Readonly<IState>): void
    {
        const { store } = this.state;

        const dots = $("li[role='presentation']").has(".dot");

        if(prevState.amountOfDots < dots.length)
        {
            this.setState({ amountOfDots: dots.length });

            dots.each((index, object) =>
            {
                object.removeEventListener("mouseenter", () => cursorActions.hoverCursors(store));
                object.removeEventListener("mouseleave", () => cursorActions.unHoverCursors(store));
            });

            dots.each((index, object) =>
            {
                object.addEventListener("mouseenter", () => cursorActions.hoverCursors(store), { passive: true });
                object.addEventListener("mouseleave", () => cursorActions.unHoverCursors(store), { passive: true });
            });
        }
    }

    public render(): React.ReactElement
    {
        const { store } = this.state;

        return (
            <Fragment>
                <div id={"main"}>
                    <Cursor store={store}/>
                    <Header store={store}/>
                    <main>
                        <CookieBanner store={store}/>
                        <LandingPage/>
                        <AboutUsText className={styles.aboutUs}/>
                        <Leistungen store={store} className={styles.leistungen}/>
                        <LiteDesk store={store} className={styles.liteDeskWrapper}/>
                        <WebProjekte store={store} className={styles.webProjekte}/>
                        <WarumDualmeta store={store} className={styles.warumDualmeta}/>
                        <Stack className={styles.stack}/>
                        <Yafa className={styles.yafa} store={store}/>
                        <RezensionenWrapper store={store} className={styles.rezensionen}/>
                        <Quote
                            className={styles.quote}
                            text={"Große Firmen bauen auf großartigen Produkten auf."}
                            author={"Elon Musk"}
                        />
                        <Kontakt store={store} className={styles.kontakt}/>
                    </main>
                </div>
                <div id={"ie11"}>
                    <p>Ihr Webbrowser (Internet Explorer) ist veraltet und wird von uns bewusst nicht mehr unterstützt. <br/> Bitte aktualisieren Sie auf Microsoft Edge oder verwenden Sie einen anderen Browser, um die Inhalte dieser Seite zu sehen.</p>
                </div>
            </Fragment>
        );
    }
}

export default HomePage;
