import React, { FunctionComponent, useEffect, useRef } from "react";

import refActions from "../../../actions/refActions";
import { EScrollPoint } from "../../../utils/enums";
import { IClassProps } from "../../../utils/interfaces";
import { IStoreProps } from "../../../utils/store";
import { Footer } from "../../global/footer/Footer";
import { eHeadingType, SectionHeading } from "../../global/sectionHeading/SectionHeading";
import { ContactBox } from "./contactBox/ContactBox";

const styles = require("./Kontakt.module.sass");

interface IProps extends IClassProps, IStoreProps {}

export const Kontakt: FunctionComponent<IProps> = props =>
{
    const ref = useRef(null);

    useEffect(() =>
    {
        refActions.addRef(props.store, ref);
    }, []);

    return (
        <div className={props?.className} ref={ref} id={EScrollPoint.kontakt}>
            <div className={styles.wrapper}>
                <SectionHeading headline={"Kontakt"} subHeadline={{ text: <p>Nehmen Sie jetzt Kontakt mit uns auf</p> }}
                    type={eHeadingType.horizontalLine}/>
                <ContactBox store={props.store}/>
            </div>
            <Footer className={styles.footer} store={props.store}/>
        </div>
    );
};
