import React, { FunctionComponent } from "react";

import { IStore } from "../../../../../utils/store";
import { Button, eButtonType } from "../../../../global/button/Button";
import { Icon } from "../../../../global/icon/Icon";

const styles = require("./InnerBox.module.sass");

interface IProps
{
    icon: string;
    heading: string;
    text: string;
    buttonText: string;
    buttonIcon?: string;
    buttonLink: string;
    store: IStore;
}

export const InnerBox: FunctionComponent<IProps> = props => (
    <div className={styles.wrapper}>
        <Icon icon={props.icon} className={styles.icon}/>
        <h3 className={styles.heading}>{props.heading}</h3>
        <p className={styles.text}>{props.text}</p>
        <Button type={eButtonType.filled} text={props.buttonText} className={styles.button} icon={props.buttonIcon} withLink={true} target={props.buttonLink} store={props.store}/>
    </div>
);
