import React, { FunctionComponent } from "react";

import { IStoreProps } from "../../../../utils/store";
import { DummyBrowserControlsBar } from "../../../global/dummyBrowserControlsBar/DummyBrowserControlsBar";
import { InnerBox } from "./innerBox/InnerBox";

const styles = require("./ContactBox.module.sass");

export const ContactBox: FunctionComponent<IStoreProps> = props =>
    <div className={styles.wrapper}>
        <DummyBrowserControlsBar className={styles.topBar}/>
        <div className={styles.boxContent}>
            <InnerBox
                icon={"phone"}
                heading={"Telefontermin"}
                text={"Vereinbaren Sie einen Termin für ein kostenloses und unverbindliches Erstgespräch mit uns am Telefon."}
                buttonText={"Termin vereinbaren"}
                buttonIcon={"date_range"}
                buttonLink={"https://calendly.com/dualmeta/termin"}
                store={props.store}
            />
            <InnerBox
                icon={"message"}
                heading={"Whatsapp"}
                text={"Sie möchten uns näher kennenlernen oder haben noch weitere Fragen? Schreiben Sie uns einfach eine Whatsapp Nachricht."}
                buttonText={"Nachricht senden"}
                buttonIcon={"message"}
                buttonLink={"https://wa.me/4915223687751"}
                store={props.store}
            />
            <InnerBox
                icon={"email"}
                heading={"E-Mail"}
                text={"Sie bevorzugen den klassischen Erstkontakt per E-Mail? Kein Problem! Schreiben Sie uns einfach an hello@dualmeta.io"}
                buttonText={"E-Mail schreiben"}
                buttonIcon={"email"}
                buttonLink={"mailto:hello@dualmeta.io"}
                store={props.store}
            />
        </div>
    </div>;
