import React, { FunctionComponent } from "react";

const styles = require("./YafaSlide.module.sass");

interface IProps
{
    img: string;
    imgWebP: string;
}

export const YafaSlide: FunctionComponent<IProps> = props => (
    <div className={styles.whiteSpaceWrapper}>
        <div className={styles.screenshotWrapper}>
            <picture>
                <source srcSet={props.imgWebP} type="image/webp"/>
                <source srcSet={props.img} type="image/jpg"/>
                <img draggable={false} className={styles.img} src={props.img} alt="Fitness App Screenshot"/>
            </picture>
        </div>
    </div>
);
