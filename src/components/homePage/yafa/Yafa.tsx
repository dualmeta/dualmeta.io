import React, { FunctionComponent } from "react";

import AddSchedule from "../../../assets/yafa/Add_Schedule.png";
import AddScheduleWebP from "../../../assets/yafa/Add_Schedule.webp";
import Exercise from "../../../assets/yafa/Exercise.png";
import ExerciseWebP from "../../../assets/yafa/Exercise.webp";
import Login from "../../../assets/yafa/Login.png";
import LoginWebP from "../../../assets/yafa/Login.webp";
import Pause from "../../../assets/yafa/Pause.png";
import PauseWebP from "../../../assets/yafa/Pause.webp";
import SaveStats from "../../../assets/yafa/Save_Stats.png";
import SaveStatsWebP from "../../../assets/yafa/Save_Stats.webp";
import ScheduleDay from "../../../assets/yafa/Schedule_Day.png";
import ScheduleOverview from "../../../assets/yafa/Schedule_Overview.png";
import ScheduleOverviewWebP from "../../../assets/yafa/Schedule_Overview.webp";
import SideMenu from "../../../assets/yafa/Side_Menu.png";
import SideMenuWebP from "../../../assets/yafa/Side_Menu.webp";
import Splash from "../../../assets/yafa/Splash.png";
import SplashWebP from "../../../assets/yafa/Splash.webp";
import { IClassProps } from "../../../utils/interfaces";
import { IStoreProps } from "../../../utils/store";
import { eHeadingType, SectionHeading } from "../../global/sectionHeading/SectionHeading";
import { SlickSliderWrapper } from "../../global/slickSliderWrapper/SlickSliderWrapper";
import { YafaSlide } from "./yafaSlide/YafaSlide";

const styles = require("./Yafa.module.sass");

interface IProps extends IStoreProps, IClassProps {}

export const Yafa: FunctionComponent<IProps> = props => (
    <div className={props?.className}>
        <SectionHeading
            headline={"Showcase"}
            subHeadline={{
                text: (
                    <p style={{ fontSize: 15 }}>
                        <b style={{
                            fontWeight: 500,
                            color: "white",
                            fontSize: 19
                        }}><i className={styles.i}>YetAnotherFitnessApp</i></b>- Eine App, mit der es möglich ist, seinen<br/>
                        Trainingsplan mit Freunden zu teilen und gegeneinander anzutreten
                    </p>
                ),
                responsive: {
                    responsiveText: (
                        <p><b style={{
                            fontWeight: 500,
                            color: "white",
                        }}><i className={styles.i}>YetAnotherFitnessApp</i></b>- Eine App zum Teilen von Trainingsplänen</p>
                    ),
                    breakpoint: 1000
                }
            }}
            type={eHeadingType.horizontalLine}
            className={styles.heading}
        />
        <SlickSliderWrapper
            store={props.store}
            arrowsMargin={"2.5%"}
            dotsTranslateY={-15}
            sliderContentAreaClassName={styles.slider}
            gradientOverlay={{
                colorFrom: "rgba(32,38,46,0.93)",
                colorTo: "rgba(32,38,46,0)",
                rightAndLeftSideWidth: "15%"
            }}
            options={{
                autoplay: true,
                autoplaySpeed: 3000,
                centerMode: true,
                pauseOnHover: false,
                centerPadding: "8%",
                draggable: false,
                infinite: true,
                dots: true,
                slidesToShow: 7,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 2200,
                        settings: {
                            slidesToShow: 6,
                            centerPadding: "8%",
                        }
                    },
                    {
                        breakpoint: 1920,
                        settings: {
                            slidesToShow: 5,
                            centerPadding: "8%",
                        }
                    },
                    {
                        breakpoint: 1500,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 1180,
                        settings: {
                            draggable: true,
                            slidesToShow: 3,
                            centerPadding: "12%",
                        }
                    },
                    {
                        breakpoint: 900,
                        settings: {
                            draggable: true,
                            slidesToShow: 2,
                            centerPadding: "16%",
                        }
                    },
                    {
                        breakpoint: 640,
                        settings: {
                            draggable: true,
                            slidesToShow: 1,
                            centerPadding: "25%",
                        }
                    },
                    {
                        breakpoint: 530,
                        settings: {
                            draggable: true,
                            slidesToShow: 1,
                            centerPadding: "23%",
                        }
                    },
                    {
                        breakpoint: 420,
                        settings: {
                            draggable: true,
                            slidesToShow: 1,
                            centerPadding: "18%",
                        }
                    }
                ]
            }}
            uniqueSliderClass={"yafa-slider-wrapper"}>
            <YafaSlide img={Splash} imgWebP={SplashWebP}/>
            <YafaSlide img={Login} imgWebP={LoginWebP}/>
            <YafaSlide img={ScheduleOverview} imgWebP={ScheduleOverviewWebP}/>
            <YafaSlide img={ScheduleDay} imgWebP={ScheduleOverviewWebP}/>
            <YafaSlide img={AddSchedule} imgWebP={AddScheduleWebP}/>
            <YafaSlide img={Exercise} imgWebP={ExerciseWebP}/>
            <YafaSlide img={SaveStats} imgWebP={SaveStatsWebP}/>
            <YafaSlide img={Pause} imgWebP={PauseWebP}/>
            <YafaSlide img={SideMenu} imgWebP={SideMenuWebP}/>
        </SlickSliderWrapper>
    </div>
);

