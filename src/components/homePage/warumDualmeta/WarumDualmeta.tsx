import React, { FunctionComponent, useEffect, useRef } from "react";

import refActions from "../../../actions/refActions";
import { EScrollPoint } from "../../../utils/enums";
import { IClassProps } from "../../../utils/interfaces";
import { IStoreProps } from "../../../utils/store";
import { eHeadingType, SectionHeading } from "../../global/sectionHeading/SectionHeading";
import { StandardBox } from "./standardBox/StandardBox";

const styles = require("./WarumDualmeta.module.sass");

interface IProps extends IClassProps, IStoreProps {}

export const WarumDualmeta: FunctionComponent<IProps> = props =>
{
    const ref = useRef(null);

    useEffect(() =>
    {
        refActions.addRef(props.store, ref);
    }, []);

    return (
        <div className={[styles.wrapper, props?.className].join(" ")} ref={ref} id={EScrollPoint.warumDualmeta}>
            <SectionHeading headline={"Vorteile"} subHeadline={{ text: <p>Davon profitieren Sie bei einer Zusammenarbeit mit uns</p> }}
                type={eHeadingType.horizontalLine}/>
            <div className={styles.boxWrapper}>
                <StandardBox heading={"Höchster Anspruch an Qualität"} icon={"whatshot"}>
                    Wir legen großen Wert auf einwandfrei funktionierende Software, benutzerfreundliche Oberflächen und
                    ein <b>hochwertiges Design.</b> Außerdem haben aktuelle Themen
                    wie <b>Datenschutz</b> oder <b>Security</b> einen hohen Stellenwert.
                </StandardBox>
                <StandardBox heading={"Strukturiertes Arbeiten"} icon={"bar_chart"}>
                    Für ein erfolgreiches Projekt ist die <b>sorgfältige Planung</b> im Voraus ein wichtiger Faktor.
                    Gemeinsam erarbeiten wir einen <b>strukturierten Projektablauf</b> und halten Sie in jeder Phase auf dem
                    aktuellen Stand!
                </StandardBox>
                <StandardBox heading={"Zuverlässigkeit"} icon={"speed"}>
                    Das <b>Einhalten von Deadlines</b> und <b>zuverlässiges Arbeiten</b> ist für uns selbstverständlich. Wir
                    machen ausschließlich <b>realistische Versprechen</b> und sorgen so für eine <b>angenehme
                    Zusammenarbeit.</b>
                </StandardBox>
                <StandardBox heading={"Clean Code"} icon={"code"}>
                    Unser Code ist sauber, <b>strukturiert</b> und sehr gut dokumentiert. Durch den Einsatz von Typisierung,
                    und einer <b>übersichtlichen Projektstruktur</b> wird das Arbeiten mit anderen Entwicklern
                    so <b>effizient</b> wie möglich.
                </StandardBox>
                <StandardBox heading={"Aktuelle Technologien"} icon={"sync"}>
                    Durch den Einsatz von <b>aktuellen und etablierten Frameworks</b> setzen wir Ihr Projekt möglichst
                    effizient und mit den neusten Methoden um. Dies <b>spart Kosten</b> und sorgt für eine <b>lange
                    Lebensdauer</b> Ihres Projekts.
                </StandardBox>
                <StandardBox heading={"Jederzeit erreichbar"} icon={"check"}>
                    Sie arbeiten gerne über die regulären Geschäftszeiten hinaus? Kein Problem! Wir legen großen Wert auf
                    eine <b>schnelle Kommunikation</b> und sind daher bei Bedarf auch am <b>Wochenende</b> für
                    Sie <b>erreichbar!</b>
                </StandardBox>
            </div>
        </div>
    );
};
