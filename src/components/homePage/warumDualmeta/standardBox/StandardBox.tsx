import React, { FunctionComponent } from "react";

import { Icon } from "../../../global/icon/Icon";

const styles = require("./StandardBox.module.sass");

interface IProps
{
    icon?: string;
    heading: string;
    className?: string;
}

export const StandardBox: FunctionComponent<IProps> = props =>
{
    const icon = <Icon icon={props.icon} className={styles.icon}/>;

    return <div className={ props.className ? [styles.wrapper, props.className].join(" ") : styles.wrapper}>
        <div className={styles.headingWrapper}>
            {props.icon ? icon : null}
            <h3 className={styles.heading}>{props.heading}</h3>
        </div>
        <p>{props.children}</p>
    </div>;
};
