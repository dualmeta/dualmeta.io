import React, { FunctionComponent, useEffect, useRef } from "react";

import refActions from "../../../actions/refActions";
import Aufgabe from "../../../assets/liteDesk/Aufgabe-min.jpg";
import AufgabeWebP from "../../../assets/liteDesk/Aufgabe-min.webp";
import Dashboard from "../../../assets/liteDesk/Dashboard-min.jpg";
import DashboardWebP from "../../../assets/liteDesk/Dashboard-min.webp";
import Dualmeta from "../../../assets/liteDesk/Dualmeta-min.jpg";
import Login from "../../../assets/liteDesk/Login-min.jpg";
import LoginWebP from "../../../assets/liteDesk/Login-min.webp";
import MaxMustermann from "../../../assets/liteDesk/MaxMustermann-min.jpg";
import MaxMustermannWebP from "../../../assets/liteDesk/MaxMustermann-min.webp";
import Projekte from "../../../assets/liteDesk/Projekte-min.jpg";
import ProjekteWebP from "../../../assets/liteDesk/Projekte-min.webp";
import YAFA from "../../../assets/liteDesk/YAFA-min.jpg";
import YAFAWebP from "../../../assets/liteDesk/YAFA-min.webp";
import { EScrollPoint } from "../../../utils/enums";
import { IClassProps } from "../../../utils/interfaces";
import { IStoreProps } from "../../../utils/store";
import { eHeadingType, SectionHeading } from "../../global/sectionHeading/SectionHeading";
import { SlickSliderWrapper } from "../../global/slickSliderWrapper/SlickSliderWrapper";
import { LiteDeskSlide } from "./liteDeskSlide/LiteDeskSlide";

const styles = require("./LiteDesk.module.sass");

interface IProps extends IStoreProps, IClassProps {}

export const LiteDesk: FunctionComponent<IProps> = props =>
{
    const ref = useRef(null);

    useEffect(() =>
    {
        refActions.addRef(props.store, ref);
    }, []);

    const reflectionHeight = 120;
    const reflectionMargin = 35;

    return (
        <div className={props?.className} ref={ref} id={EScrollPoint.litedesk}>
            <SectionHeading
                headline={"Referenzen"}
                subHeadline={{ text: <p>"liteDesk" - Ein Projektmanagement Tool entwickelt von Dualmeta</p> }}
                type={eHeadingType.horizontalLine}
                className={styles.heading}
            />
            <SectionHeading
                headline={"Referenzen"}
                subHeadline={{ text: <p>"liteDesk" - Ein Projektmanagement Tool entwickelt von Dualmeta</p> }}
                type={eHeadingType.horizontalLine}
                className={styles.headingRespo}
            />
            <SlickSliderWrapper
                store={props.store}
                dotsTranslateY={-(reflectionHeight + reflectionMargin - 25)}
                arrowsMargin={"2.5%"}
                arrowsTranslateY={(reflectionHeight + reflectionMargin) / 2}
                sliderClassName={styles.sliderOverrides}
                gradientOverlay={{
                    colorFrom: "rgba(32,38,46,0.93)",
                    colorTo: "rgba(32,38,46,0)",
                    rightAndLeftSideWidth: "23%"
                }}
                options={{
                    autoplay: true,
                    autoplaySpeed: 3000,
                    centerMode: true,
                    centerPadding: "25%",
                    infinite: true,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 1600,
                            settings: {
                                centerPadding: "20%",
                            }
                        },
                        {
                            breakpoint: 1200,
                            settings: {
                                centerPadding: "18%",
                            }
                        },
                        {
                            breakpoint: 1000,
                            settings: {
                                centerPadding: "16%",
                            }
                        },
                        {
                            breakpoint: 800,
                            settings: {
                                centerPadding: "14%",
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                centerPadding: "8%",
                            }
                        }
                    ]
                }}
                uniqueSliderClass={"liteDesk-slider-wrapper"}>
                <LiteDeskSlide reflectionHeight={reflectionHeight} reflectionMargin={reflectionMargin} img={Login} imgWebP={LoginWebP} store={props.store}/>
                <LiteDeskSlide reflectionHeight={reflectionHeight} reflectionMargin={reflectionMargin} img={Projekte} imgWebP={ProjekteWebP} store={props.store}/>
                <LiteDeskSlide reflectionHeight={reflectionHeight} reflectionMargin={reflectionMargin} img={Dashboard} imgWebP={DashboardWebP} store={props.store}/>
                <LiteDeskSlide reflectionHeight={reflectionHeight} reflectionMargin={reflectionMargin} img={Dualmeta} imgWebP={DashboardWebP} store={props.store}/>
                <LiteDeskSlide reflectionHeight={reflectionHeight} reflectionMargin={reflectionMargin} img={MaxMustermann} imgWebP={MaxMustermannWebP} store={props.store}/>
                <LiteDeskSlide reflectionHeight={reflectionHeight} reflectionMargin={reflectionMargin} img={YAFA} imgWebP={YAFAWebP} store={props.store}/>
                <LiteDeskSlide reflectionHeight={reflectionHeight} reflectionMargin={reflectionMargin} img={Aufgabe} imgWebP={AufgabeWebP} store={props.store}/>
            </SlickSliderWrapper>
        </div>
    );
};

