import React, { FunctionComponent } from "react";

import cursorActions from "../../../../actions/cursorActions";
import { IStore } from "../../../../utils/store";
import { DummyBrowserControlsBar } from "../../../global/dummyBrowserControlsBar/DummyBrowserControlsBar";

const styles = require("./LiteDeskSlide.module.sass");

interface IProps
{
    img: string;
    imgWebP: string;
    reflectionHeight: number;
    reflectionMargin: number;
    store: IStore;
}

export const LiteDeskSlide: FunctionComponent<IProps> = props =>
{
    const reflectionGradientColorFrom = "rgba(32, 38, 46, 0.8)";
    const reflectionGradientColorTo = "rgba(32, 38, 46, 1)";

    return (
        <div className={styles.whiteSpaceWrapper}>
            <div className={styles.slideWrapper}>
                <div className={styles.screenshotWrapper}>
                    <DummyBrowserControlsBar/>
                    <picture>
                        <source srcSet={props.imgWebP} type="image/webp"/>
                        <source srcSet={props.img} type="image/jpg"/>
                        <img className={styles.img} src={props.img} alt="liteDesk Projektmanagement-Tool Screenshot"/>
                    </picture>
                </div>
                <div
                    style={{
                        marginTop: `${props.reflectionMargin}px`,
                        height: `${props.reflectionHeight}px`,
                    }}
                    className={styles.reflection}>
                    <picture>
                        <source srcSet={props.imgWebP} type="image/webp"/>
                        <source srcSet={props.img} type="image/jpg"/>
                        <img className={styles.reflectionImg} src={props.img} alt="liteDesk Projektmanagement-Tool Screenshot"/>
                    </picture>
                    <div
                        style={{ background: `transparent linear-gradient(180deg, ${reflectionGradientColorFrom} 14%, ${reflectionGradientColorTo} 100%) 0 0 no-repeat padding-box` }}
                        className={styles.reflectionOverlay}
                    />
                </div>
            </div>
        </div>
    );
};

