import React from "react";

import { EWebPSupport } from "./enums";

export interface IStore
{
    webPSupport: EWebPSupport;
    isCursorHovered: boolean;
    refs: React.RefObject<HTMLElement>[];
    subscribers?: ((store: IStore) => void)[];
    notify?(): void;
}

export interface IStoreProps
{
    store: IStore;
}

export const createStore = (): IStore => ({
    webPSupport: null,
    isCursorHovered: false,
    refs: [],
    subscribers: [],
    notify()
    {
        this.subscribers.forEach((subscriber: (store: IStore) => void) =>
            subscriber(this)
        );
    }
});

