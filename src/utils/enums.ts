export enum EScrollPoint { leistungen = "leistungen", warumDualmeta = "warumDualmeta", litedesk = "litedesk", rezensionen = "rezensionen", kontakt = "kontakt" }

export enum ECursorRef { normalCursor = "normalCursor", laggyCursor = "laggyCursor" }

export enum EWebPSupport { notSupported, supported }
