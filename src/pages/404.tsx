import React, { Fragment } from "react";

import { Page404 } from "../components/404/404.page";
import SEO from "../components/SEO";
import { createStore } from "../utils/store";

import "../styles/style.sass";

export default (): React.ReactElement => (
    <Fragment>
        <SEO title={"Page not found | Dualmeta"}/>
        <Page404 store={createStore()}/>
    </Fragment>
);

