import React, { Fragment } from "react";

import { ImpressumPage } from "../components/impressum/Impressum.page";
import SEO from "../components/SEO";
import { createStore } from "../utils/store";

export default (): React.ReactElement => (
    <Fragment>
        <SEO title={"Impressum | Dualmeta"}/>
        <ImpressumPage store={createStore()}/>
    </Fragment>
);
