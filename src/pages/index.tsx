import React, { Fragment } from "react";

import HomePage from "../components/homePage/Home.page";
import SEO from "../components/SEO";
import { createStore } from "../utils/store";

import "../styles/style.sass";

export default (): React.ReactElement => (
    <Fragment>
        <SEO/>
        <HomePage store={createStore()}/>
    </Fragment>
);
