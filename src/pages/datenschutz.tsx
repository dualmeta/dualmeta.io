import React, { Fragment } from "react";

import { DatenschutzPage } from "../components/datenschutz/Datenschutz.page";
import SEO from "../components/SEO";
import { createStore } from "../utils/store";

export default (): React.ReactElement => (
    <Fragment>
        <SEO title={"Datenschutz | Dualmeta"}/>
        <DatenschutzPage store={createStore()}/>
    </Fragment>
);
