import React, { Fragment } from "react";

import { AgbPage } from "../components/agb/Agb.page";
import SEO from "../components/SEO";
import { createStore } from "../utils/store";

export default (): React.ReactElement => (
    <Fragment>
        <SEO title={"AGB | Dualmeta"}/>
        <AgbPage store={createStore()}/>
    </Fragment>
);
